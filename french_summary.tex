\chapter*{Résumé}

Les quasicristaux sont des solides apériodiques présentant un ordre à longue distance, qui se manifeste par une figure de diffraction purement constituée de pics de Bragg.
Dans de tels matériaux, ordonnés mais non périodiques, quelle est la structure du spectre et des fonctions d'onde des électrons de conduction ?
C'est l'objet de cette thèse de proposer des réponses à cette question générale. 

\begin{figure}[htp]
\centering
\includegraphics[width=0.5\textwidth]{img0/penrose_sutherland_state}
\caption{Un état propre fractal d'un modèle de la forme \eqref{eq:intro}. Le pavage quasipériodique sous-jacent est le pavage de Penrose. Il représenté par le graphe blanc: chaque sommet correspond à un site $m$ dans l'équation \eqref{eq:intro}, et chaque arrête correspond à un lien $t_{m,n}$ entre deux sites. La couleur code la probabilité de présence de l'électron (la couleur a été ``élargie'' au voisinage de chaque site pour plus de visibilité).}
\label{fig:sutherland_state}
\end{figure}

Dans cette thèse, les quasicristaux sont modélisé par des pavages quasipériodiques uni ou bidimensionnels.
Sur ces pavages, nous étudions les propriétés d'un unique électron de conduction. 
Le hamiltonien de l'électron, de type liaisons fortes, est donné par
\begin{equation}
\label{eq:intro}
	\op{H} = - \sum_{\langle m, n \rangle} t_{m,n} \ket{m} \bra{n} + \hc + \sum_m V_m \ket{m}\bra{m},
\end{equation}
où les entiers $m$ et $n$ indexent les atomes, et où $\ket{m}$ est la fonction d'onde localisée sur l'atome numéro $m$.
$t_{m,n}$ est l'amplitude de saut de l'atome $m$ à l'atome $n$, et $V_m$ est un potentiel sur site.
Les $t_{m,n}$ et $V_m$ sont choisis de façon à respecter la symétrie du pavage quasipériodique sous-jacent.
De tels modèles ont des \emph{propriétés critiques}: leur spectre possède une composante singulière, et leurs états propres sont génériquement multifractals. 
La figure \eqref{fig:sutherland_state} montre un exemple d'un tel état propre, pour un modèle de type \eqref{eq:intro} introduit par \cite{Sutherland}.
Sur l'échantillon choisi ici, la distribution des amplitudes adopte une structure en forme d'étoile à cinq branches, qui est elle-même constituée de structures similaires, à une plus courte échelle. 
Cette ``invariance d'échelle'' de la distribution locale des amplitudes est le reflet de la nature multifractale de l'état électronique.


\begin{figure}[htp]
\centering
\includegraphics[width=0.8\textwidth]{img0/cut_and_project.pdf}
\caption{Illustration de la méthode de coupe et projection, utilisée ici pour construire la chaîne de Fibonacci $\tau = (1+\sqrt{5})/2$ est le nombre d'or.}
\label{fig:cut_and_project}
\end{figure}

Dans une première partie, nous introduisons les pavages quasipériodiques d'un point de vue géométrique, et présentons deux méthodes de construction : la méthode de coupe et projection, et les règles de substitution.
Attardons-nous un instant pour décrire la méthode de coupe et projection.
Grossièrement, elle consiste en la projection d'une ``tranche'' de points d'un réseau de dimension $D$ sur un sous-espace de dimensions $d < D$.
Cette opération  permet de construire un pavage périodique ou quasipériodique.
La figure \eqref{fig:cut_and_project} illustre cette construction dans le cas où on part d'un réseau carré ($D=2$). 
La ``tranche'' est ici obtenue en translatant le carré unité (en bas à  gauche de la figure) le long de la droite de pente $\tau^{-1}$, où $\tau$ est nombre d'or.
Les points de la ``tranche'' sont en bleu sur la figure. 
Ils sont projetés sur cette même droite, formant une chaîne de points.
La distance entre deux points sur cette chaîne peut être ``petite'' (indiquée par un double lien sur la figure), ou ``grande'' (simple lien) de leurs plus proches voisins.
Cette chaîne quasipériodique est appelé chaîne de Fibonacci.

Nous considèrerons en fait dans cette thèse qu'un pavage est quasipériodique si et seulement si il n'est pas périodique, et peut être construit par la méthode de coupe et projection décrite ci-dessus.
Nous présentons dans le même temps quelques propriétés des pavages quasipériodiques : la répétitivité, l'ordre à longue distance et l'invariance d'échelle.
Dans le cas unidimensionnel, nous introduisons la notion de complexité qui nous sert à appuyer l'idée que les pavages quasipériodiques sont les pavages apériodiques les plus proches de la périodicité.


\begin{figure}[htp]
\centering
  \includegraphics[width=.5\textwidth]{img1/ldos_reordered.pdf}
\caption{Densité d'état électronique, en fonction de la ``co-numérotation'' (conumber en anglais) et de l'énergie. Une structure fractale est clairement visible}
\label{fig:fractale_de_noel}
\end{figure}

Dans une deuxième partie, nous nous concentrons plus particulièrement sur l'un de pavages quasipériodiques les plus simples, la chaîne de Fibonacci (figure \eqref{fig:fractale_de_noel}).
Sur cette chaîne, les modèles de type \eqref{eq:intro} possèdent un spectre purement singulier (la densité d'état intégrée est un escalier de Cantor), et des états propres critiques (c'est-à-dire ni localisés ni étendus, mais multifractals).
À l'aide d'un groupe de renormalisation, on peut exploiter la construction par substitution de la chaîne de Fibonacci (non explicitée dans ce résumé), pour montrer le caractère singulier du spectre.
Concrètement, le groupe de renormalisation permet de montrer que $\op{H}(L)$, le hamiltonien du système sur un chaîne de taille $L$, s'écrit
\begin{equation}
\label{eq:récurrence}
	\op{H}(L) \simeq \left( z \op{H}(L/\tau^{2}) - t_\B \right) \oplus \left( \zb \op{H}(L/\tau^3) \right) \oplus \left( z \op{H}(L/\tau^2) + t_\B \right),
\end{equation}
où $t_\B$, $z$ et $\zb$ sont des paramètres physiques.
On voit ainsi que le hamiltonien à l'échelle $L$ peut être décrit comme une somme de hamiltoniens à de plus courtes échelles $L/\tau^2$, $L/\tau^3$.
Comme ces hamiltoniens sont similaires au hamiltonien d'origine, cette équation révèle une propriété d'invariance d'échelle du hamiltonien.
De cela découle le caractère singulier du spectre.

En utilisant ce même groupe de renormalisation, nous dérivons une relation de récurrence perturbative pour les états électroniques, similaire à l'équation \eqref{eq:récurrence}.
Nous montrons à l'aide de cette équation que les états propres sont multifractals, et calculons leurs dimensions fractales. 
La figure \eqref{fig:fractale_de_noel} montre le caractère fractal de la densité électronique.
La ``co-numérotation'' est une façon de réarranger les positions atomiques, qui peut être considérée comme une sorte d'équivalent de la transformation de Fourier pour un cristal périodique.
On peut donc considérer la figure \eqref{fig:fractale_de_noel} comme l'équivalent pour notre chaîne quasipériodique d'une représentation de la densité électronique dans la zone de Brillouin, et en fonction de l'énergie, pour une chaîne périodique.

\begin{figure}[htp]
		\centering
		\includegraphics[width=.6\textwidth]{img2/loggapwidth_Fibonacci_l_16.pdf}
	\caption{Largeur des gaps spectraux d'une chaîne périodique approchant la chaîne quasipériodique de Fibonacci, en fonction du l'indice des gaps.}
		\label{fig:gapwidth}
\end{figure}

Dans une troisième partie, élargissant ensuite notre champ d'étude à un ensemble de chaînes quasipériodiques, nous nous intéressons au théorème d'indexation des gaps, qui décrit comment la géométrie d'une chaîne donnée contraint les valeurs que peut prendre la densité d'états intégrée dans les gaps du spectre électronique.
Ainsi par exemple, pour la chaîne de Fibonacci, le théorème prédit que la densité d'états de tout Hamiltonien ``raisonnable'' ne peut prendre dans les gaps que les valeurs
\begin{equation}
	\text{idos} = n \tau^{-1} \mod 1
\end{equation}
où $n$ est entier, et $\tau = (1+\sqrt{5})/2$ est le nombre d'or.
Ainsi, tout gap peut être indexé de manière unique par un entier $n$, qui s'avère être un nombre d'enroulement topologique.

Nous nous intéressons à la façon dont l'énoncé de ce théorème est modifié lorsque l'on considère une séquence d'approximants périodiques approchant une chaîne quasipériodique. 
Nous montrons dans ce contexte qu'on peut distinguer des gaps stables restant ouverts à la limite quasipériodique, et des gaps transitoires, qui se ferment à la limite quasipériodiques.
Le caractère stable/transient d'un gap est donné par la valeur de son indice $n$: les gaps d'indice trop grand (en rouge sur la figure \eqref{fig:gapwidth}) étant transitoires.

\begin{figure}[htp]
		\centering
		\includegraphics[width=.4\textwidth]{img3/heights_small_patch.pdf}
	\caption{Champ de flèches et champ de hauteurs sur un morceau du pavage quasipériodique d'Ammann-Beenker.}
		\label{fig:flèches}
\end{figure}

Enfin, dans une dernière partie, nous montrons comment des champs de hauteurs géométriques peuvent être utilisés pour construire des états électroniques exacts sur des pavages en une et deux dimensions.
Plus précisément, nous définissons tout d'abord sur le pavage un champ de flèches irrotationnel, comme illustré sur la figure \eqref{fig:flèches}.
Nous imposons que ce champ de flèches soit \emph{local}, c'est-à-dire défini par l'agencement local des tuiles. 
Nous intégrons ensuite ce champ de flèches, obtenant ainsi une quantité scalaire définie en chaque sommet du graphe du pavage, que nous appelons le champ de hauteur, $h$.
Nous utilisons alors ce champ de hauteur pour construire une fonction d'onde, sous la forme
\begin{equation}
\label{eq:skk}
	\psi(m) = e^{\kappa h(m)},
\end{equation}
où $\kappa$ est un paramètre libre. 
Nous montrons que cette fonction d'onde est état propre de hamiltoniens de la forme \eqref{eq:intro}, pour les chaînes quasipériodiques construires par coupe et projection, et pour les pavages quasipériodiques bidimensionnels de Penrose (figure \eqref{fig:sutherland_state}) et d'Ammmann-Beenker (figure \eqref{fig:flèches}).

De façon remarquable, la localité du champ de flèches n'implique pas celle du champ de hauteur : il peut être distribué indépendamment de l'environnement local.
C'est ce qu'illustre la figure \eqref{fig:sutherland_state}: l'état représenté (qui est de type \eqref{eq:skk}) a une structure invariante d'échelle (donc non-locale), découplée de l'environnement local donné par le pavage sous-jacent.
Quand ce découplage se produit, tout état $\psi(m) = e^{\kappa h(m)}$ construit avec le champ de hauteur $h$ est multifractal.


\begin{figure}[htp]
	\centering
	\includegraphics[scale=0.7]{img3/8_harmonic_mean_transmission_fixed_origin.pdf}
	\caption{Moyenne harmonique de la conductivité au niveau de Fermi  d'un échantillon de taille $L$ de la chaîne de Fibonacci. Tirets: prédiction théorique, ligne: calcul numérique.}
	\label{fig:conductivité}
\end{figure}

Ces états sont robustes aux perturbations du hamiltonien, sous réserve que ces dernières respectent les symétries du pavage sous-jacent. 
Nous relions les dimensions fractales de ces états à la distribution de probabilités des hauteurs, que nous calculons de façon exacte. 
Dans le cas des chaînes quasipériodiques, nous montrons que la conductivité suit une loi d'échelle de la taille de l'échantillon (figure \eqref{fig:conductivité}), dont l'exposant est relié à cette même distribution de probabilités.