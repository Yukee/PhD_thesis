\begin{chabstract}
In this short chapter, we study the gap properties of nearest neighbor tight-binding models on quasiperiodic chains.
For finite chains, we argue that two kind of gaps should be distinguished: stable and transient.
We show that stable gaps have a well defined limit as the length of the chain goes to infinity. 
We also show that there is a direct relation between the gap size and the gap label.
\end{chabstract}

\chapter{Labeling gaps of quasiperiodic chains}%{Keywords: substitution chains, $\idos$}
\label{chap:gap_labeling}

\minitoc

\section{The gap labeling theorem}

The gap labeling theorem (GLT) \cite{bellissard1989, bellissard, bellissard2000hull, bellissard2001gap} is one of the few exact results known about electronic properties of one, two and three dimensional quasicrystals.
It concerns the \emph{integrated density of states} (IDOS), which is defined as $\idos(E) = \text{fraction of energy states below energy~}E$.
Inside a spectral gap, the IDOS is of course constant, and the theorem constrains the values it can take.
In the case of canonical \cp\ chain of slope $\irr$, the theorem states that
\begin{equation}
\label{eq:gl}
	\idos(E \in \text{gap}) = \frac{n}{1+\alpha} \mod 1
\end{equation}
where $n$ is an integer called the label of the gap.
Alternatively, one can write
\begin{equation}
	\idos(E \in \text{gap}) =  \frac{n}{1+\alpha} + m
\end{equation}
with $m(n)$ the integer such that the IDOS satisfies $0 \leq \idos < 1$.

The GLT can appear mysterious and profound. However, it has a simple derivation, making use of the approximants.
Let $\alpha_l = p_l/q_l$ be a convergent of $\alpha$. Convergents are the ``best'' rational approximations of a given irrational number. See the Appendix \eqref{app:sec:approximants} for more details.
Consider the periodic approximant built with $\alpha_l$.
Its spectrum consists in general of $N_l = p_l + q_l$ energy bands (see Chap.\ \ref{chap:qp_chains}, and \cite{diffractionLuck, codimSire}), and thus the IDOS inside a gap is given by
\begin{equation}
	\idos(E \in \text{gap}) = \frac{j(E)}{N_l}
\end{equation}
where $j(E)$ counts the number of bands below energy $E$.
Since $p_l$ and $q_l$ are relatively prime, using Bézout's identity we can write $j = a p_l + b q_l = (a-b) q_l + b N_l$, with $a$ and $b$ integers.
We let $n = a-b$ and we remark that
\begin{equation}
\label{eq:glapprox}
	\idos(E \in \text{gap}) = \frac{n}{1+\irr_l} \mod 1
\end{equation}
Taking the quasiperiodic limit $l \to \infty$ at fixed $n$, we recover the GLT.
Moreover, the formula \eqref{eq:glapprox} extends the GLT to approximants, which, as we will see, provides useful insight into the gap structure.
%To see it more clearly, let us consider the particular case of the Fibonacci chain. In that case, the irrational is the inverse of the golden ratio: $\irr = \omega = (\sqrt{5}-1)/2$, and the gap labeling theorem for approximants writes 
%\begin{equation}
%\label{eq:gap_labeling_fibo_approximant}
%	\idos(E\in \text{gap}) = n \frac{F_{l-1}}{F_l} \mod 1,
%\end{equation}
%while for the quasiperiodic tiling, it writes
%\begin{equation}
%	\idos(E\in \text{gap}) = n \omega \mod 1.
%\end{equation}
%Consider now the sequence of gap labels defined for approximants of the series $l = 3k$ by
%\begin{equation}
%	n_{l=3k} = \floor*{\frac{(2+\sqrt{5})^{k}}{2\sqrt{5}} + \frac{1}{2}}
%\end{equation}
%As can be checked by plugging it in formula \eqref{eq:gap_labeling_fibo_approximant}, this gap has $\idos = 1/2$. So, there is a gap at $\idos = 1/2$ for approximants of the series $l = 3k$.
%Indeed, these approximants have $F_{3k}$ bands, and as this number is odd, $\idos = 1/2$ corresponds to a gap. No problem so far!
%Now, letting $k \to \infty$, we could conclude that there is a gap at $\idos = 1/2$ in the quasiperiodic chain. This however leads to a contradiction: there is no integer $n$ such that $n \omega \mod 1 = 1/2$. So

As such, the theorem contains little information: it only constrains the set of values the IDOS can take inside the gaps, but does not tell anything about the energy location of the gaps, or about their width.
Nevertheless, as we shall see, the gap label $n$ contains extra information, and is in particular relevant for characterizing the gap widths.
This is not the first time the gap labeling theorem is given a physical meaning: in \cite{Levy2015} it is shown that gap labels can be interpreted as topological numbers, giving the winding number of edge states introduced in the gaps.

\section{Nearest neighbor tight-binding model on quasiperiodic chains}

In this chapter, we examine chains obtained by the canonical \cp\ method (see Chap.\ \ref{chap:bulding_qc}).
We consider, just as in the previous chapter, the following model Hamiltonian where the hopping amplitude takes two values according to the quasiperiodic sequence:
\begin{equation}
	\op{H}(\alpha) = -\sum_{m} t_{m} \ket{m-1} \bra{m} + \hc.
\end{equation}
We recall that $t_m = t_\A$ or $t_m = t_\B$ if the letter in between sites $m-1$ and $m$ respectively is an A or a B.
Once the irrational $\irr$ is fixed, the only free parameter in the model is the ratio of the two jump amplitudes $\rho = t_\A / t_\B$. We shall only consider, as in the previous chapter, the case where $0 < \rho < 1$.

This model can be realized experimentally with light \cite{light} or cavity polaritons \cite{tanese2014}.
In the latter case, the details of the energy spectrum could be accurately measured, leading the authors to observe subtle phenomena such as the energy displacement of an edge state within a spectral gap.
Theoretical studies of the spectral gaps are particularly relevant in these contexts.

\section{Application: the Fibonacci quasicrystal}

\begin{figure}[htp]
	\centering
	\begin{subfigure}{0.4\textwidth}
		\centering
		\includegraphics[width=1.\textwidth]{img2/gap_labels.pdf}
		\caption{\small{The gap labels of the first few Fibonacci approximants. Blue: stable gaps, red: transient gaps.}}
		\label{fig:fibogaps}
	\end{subfigure}
	%
	\begin{subfigure}{0.4\textwidth}
		\centering
		\includegraphics[width=1.\textwidth]{img2/recursive_construction_spectrum.pdf}
		\caption{\small{The recursive construction of the Fibonacci spectrum.\\}}
		\label{fig:fibospec}
	\end{subfigure}
	%
	\caption{Energy spectra of the first few Fibonacci approximants. Here we took $\rho = 0.5$, and the spectra were approximated using the renormalization group method (see Chap.\ \ref{chap:qp_chains}). On the left hand site figure, the notation $\overline{n} = -n$ was used.}
\end{figure}

We now examine in detail the special case of the Fibonacci chain, and see how the gap labeling theorem adapts to the case of finite or periodic chains.
The Fibonacci chain has slope $\alpha = \omega$, where $\omega = (\sqrt{5}-1)/2$ is the inverse of the golden ratio.
The gap labeling is thus
\begin{equation}
	\label{eq:fibogaps}
	\idos(E \in \text{gap}) = n \omega \mod 1
\end{equation}
We recall (see Sec.\ \ref{subsec:approximants} for more details) that the Fibonacci approximants are built with consecutive Fibonacci numbers: $p_l = F_{l-2}$, $q_l = F_{l-1}$.
The $l^\text{th}$ approximant has $N_l-1 = F_l -1$ gaps.
Naively, we could expect all these gaps to remain open in the quasiperiodic limit $l \to \infty$.
This is not true. 
For example, the approximants of the form $l = 3m$ have a gap at $E=0$ (Fig.\ \eqref{fig:fibogaps}), but it closes in the quasiperiodic limit, as can be seen directly on the approximants, or by the fact that there is no $n$ such that $n \omega \mod 1 = 1/2$.
We will say that such a gap is \emph{transient}.
On the contrary, the two largest gaps of the Fibonacci approximants (labeled $n = \pm 1$ see Fig.\ \eqref{fig:fibogaps}) appear to be \emph{stable}: they stay open all the way to the quasiperiodic limit.

\subsection{Renormalization group and recursive gap labeling}
The behavior of an electron of the Fibonacci chain is well understood. 
One can show (see Chap.\ \ref{chap:qp_chains}) that the spectrum of the $l^\text{th}$ approximant can be approximated by a simple linear combination of the spectra of the approximants $l-2$ and $l-3$ (Fig.\ \eqref{fig:fibospec}).
This provides us with a recursive gap labeling procedure. 
Let $\gv = (m, n)$ be the vector containing the labels of a gap \eqref{eq:gl}.
Let $G_l$ be the set of all gap labels $\gv$ of the $l^\text{th}$ approximant.
Using the recursive spectrum construction of figure \eqref{fig:fibospec} we see that $G_l$ can be partitioned into 3 subsets containing the gap labels of the left, central and right clusters of energy bands.
We have the following recursive relations:
\begin{equation}
\label{eq:gap_recursion}
\begin{aligned}
	G_{l}^{\text{left}} &= \sub^{-2} G_{l-2} \\
	G_{l}^0 &= \sub^{-3} G_{l-3} + \gv_1\\
	G_{l}^\text{right} &= \sub^{-2} G_{l-2} + \gv_2
\end{aligned}
\end{equation}
where $\gv_1 = (1, -1)$ and $\gv_2 = (0, 1)$ are the labels of the two main gaps (corresponding to $n = \pm 1$, see figure \eqref{fig:fibogaps}),
and $\sub$ is the geometrical substitution matrix
\begin{equation}
\label{eq:fibonacci_substitution_matrix}
	\sub = 
	\begin{bmatrix}
		1 & 1\\
		1 & 0\\
	\end{bmatrix}
\end{equation}
that can be used to generate the Fibonacci sequence (see Sec.\ \ref{sec:fibonacci}).
Let us prove this formula.

\begin{proof}[Recursive gap labeling for Fibonacci]
Let $g$ be the IDOS of a gap in $G_{l-2}$.
Let $g'$ be the IDOS of the unique gap in $G_l^\text{left}$ under which there are as many bands as below $g$.
In other words, $g'$ verifies
\begin{equation}
	F_l g' = F_{l-2} g.
\end{equation}
By virtue of formula \eqref{eq:glapprox}, one can write $g = n F_{l-3}/F_{l-2} + m$, and $g' = n' F_{l-1}/F_{l} + m'$.
One has therefore the following relation between gap labels:
\begin{equation}
\label{eq:explicit_gap_labels}
	m' F_l + n' F_{l-1} = m F_{l-2} + n  F_{l-3}.
\end{equation}
For $a$, $b$ integers, we introduce the following notation:
\begin{equation}
	\begin{bmatrix}
		a \\
		b \\
	\end{bmatrix}_l
	=
	a F_l + b F_{l-1}
\end{equation}
Let us call the left hand side of this equation an ``$l$-vector''.
On can pass from an $l$-vector to an $l+1$-vector by applying the Fibonacci substitution matrix $\sub$:
\begin{equation}
	\begin{bmatrix}
		a \\
		b \\
	\end{bmatrix}_{l+1}
	= 
	\left[ \sub
	\begin{pmatrix}
		a \\
		b \\
	\end{pmatrix}
	\right]_l
\end{equation}
Moreover, two $l$-vectors whose components are strictly smaller than $F_l$ in modulus are equal if an only if their components are equal:
\begin{equation}
	\begin{bmatrix}
		a \\
		b \\
	\end{bmatrix}_{l}
	= 
	\begin{bmatrix}
		a' \\
		b' \\
	\end{bmatrix}_{l}
	\Leftrightarrow
	\begin{pmatrix}
		a \\
		b \\
	\end{pmatrix}
	= 
	\begin{pmatrix}
		a' \\
		b' \\
	\end{pmatrix}
\end{equation}
This property simply follows from the fact that the two consecutive Fibonacci numbers $F_{l-1}$ and $F_l$ are coprime.
Now, the equation \eqref{eq:explicit_gap_labels} can be rewritten in $l$-vector form as
\begin{equation}
	\begin{bmatrix}
		m' \\
		n' \\
	\end{bmatrix}_{l}
	=
	\left[ \sub^{-2}
	\begin{pmatrix}
		m \\
		n \\
	\end{pmatrix}
	\right]_l
\end{equation}
which leads to the equality between vectors:
\begin{equation}
	\begin{pmatrix}
		m' \\
		n' \\
	\end{pmatrix}	
	=
	\sub^{-2}
	\begin{pmatrix}
		m \\
		n \\
	\end{pmatrix}
\end{equation}
This proves the first line of \eqref{eq:gap_recursion}. The other two lines can be proven similarly.
\end{proof}

This recursive relation provides us with a very simple geometrical interpretation of the GLT, in the simple case of the Fibonacci chain.
Let us see why.

\subsection{Transient and stable gaps of the Fibonacci chain}
As we already point out earlier on, the gap at $\idos = 1/2$ is transient.
Indeed, calling $\Delta_0$ the width of this gap at step $l = 0$ (see Fig.~\eqref{fig:fibogaps}), we see that the width of this gap at step $l = 3m$ is
\begin{equation}
	\Delta_{l = 3m} = \zb^m \Delta_0
\end{equation}
which indeed goes to 0 in the quasiperiodic limit $l \to \infty$.

\subsubsection{Transient gaps}
We say that a gap $g'$ is an iterate of a gap $g$ if $g'$ is constructed from $g$ by a RG operation. 
For example, the leftmost gap of the third approximant shown on Fig.~\eqref{fig:fibogaps} is constructed from the gap of the first approximant by a RG molecular operation; it is thus an iterate of this gap. 
Iterates verify $\Delta_l(g') = z \Delta_{l-2}(g)$ or $\Delta_l(g') = \zb \Delta_{l-3}(g)$.
By extension, we also call iterates gaps that are constructed from a given gap by several RG operations.
With this definition, it is clear that \emph{all iterates of the transient gap at $\idos = 1/2$ are also transient}. They are marked in red on Fig.~\eqref{fig:fibogaps}. 
We will show that they are the only transient gaps.

\subsubsection{Stable gaps}
The two main gaps which separate the atomic and the molecular clusters are stable.
To prove it, let us compute their width $\Delta_l$ in perturbation theory and show that it is non zero in the quasiperiodic limit.
One has
\begin{equation}
	\Delta_l = t_\B - \frac{\zb}{2} W_{l-3} - \frac{z}{2} W_{l-2}
\end{equation}
where $W_l$ is the width of the spectrum of the l$^\text{th}$ approximant. We easily check that
\begin{equation}
	\Delta_l \xrightarrow[l \to \infty]{} t_\B (1- z) +  \mathcal{O}(\rho^2)
\end{equation}
which is indeed nonzero.
Now, it is clear that all iterates of the two main gaps are also stable. 
Because all gaps are either iterates of the two main gaps or of the gap at $\idos = 1/2$, we conclude that \emph{a given gap is either an iterate of the gap at $\idos = 1/2$, and is in this case transient, or is an iterate of one of the two main gaps, and is in this case stable}. On Fig.\ \eqref{fig:fibogaps} stable gaps are marked in blue and transient gaps in red.

\subsubsection{Labeling of transient and stable gaps}
When it exists (\ie\ when $l$ is a multiple of 3), the central gap has label
\begin{equation}
	n_l = \frac{F_l}{2}.
\end{equation}
So, the label of the central gap varies from approximants to approximants, while its IDOS stays constant.
This is true for all transient gaps, since they are iterates of the central gap.
For stable gaps, it is the other way around: their labeling is fixed, while their IDOS varies from approximants to approximants (but converges to a well defined value).

\subsubsection{Conclusion to Fibonacci gap labeling}
\begin{figure}[htp]
	\centering
	\includegraphics[width=.6\textwidth]{img2/fibonacci_spectra_varying_rho.png}
	\caption{Spectrum of the approximant $l = 6$ (13 sites) as a function of the parameter $\rho$.}
	\label{fig:fibo_spectra_rho}
\end{figure}

On the example of the Fibonacci chain, we see how the GLT can be extended to the approximants, at the price of introducing transient gaps that are not present in the spectrum of the quasiperiodic system.
Transient and stable gaps can be recursively characterized: the former are the iterates by the RG of the central gap, while the latter are the iterates of the two main gaps.
We used perturbation theory in the strong quasiperiodicity limit ($\rho \ll 1$) to derive all the results above. However, the conclusions states here remain valid \emph{non-perturbatively}, as no gap closes when $\rho$ is varied between 0 and 1, as shown on Fig.\ \eqref{fig:fibo_spectra_rho}.

We now turn to the general characterization of the transient and stable gaps in the case of an arbitrary \cp\ chain.

\section{Transient and stable gaps for generic Sturmian chains}
\label{sec:transient_and_stable_gaps}

\begin{figure}[htp]
	\centering
	\begin{subfigure}{0.3\textwidth}
		\centering
		\includegraphics[width=1.\textwidth]{img2/loggapwidth_Fibonacci_l_16.pdf}
		\caption{\small{Fibonacci chain of $987$ atoms.}}
		\label{fig:width1}
	\end{subfigure}
	%
	\begin{subfigure}{0.3\textwidth}
		\centering
		\includegraphics[width=1.\textwidth]{img2/loggapwidth_Silver_l_10.pdf}
		\caption{\small{Silver mean chain of $1393$ atoms.}}
		\label{fig:width2}
	\end{subfigure}
	%
	\begin{subfigure}{0.3\textwidth}
		\centering
		\includegraphics[width=1.\textwidth]{img2/loggapwidth_sqrt3_l_12.pdf}
		\caption{\small{Chain $\alpha = \sqrt{3}-1$ of $989$ atoms.}}
		\label{fig:width3}
	\end{subfigure}
	\caption{Gap width as a function of gap label for several quasiperiodic chains. Blue: stable gap, red: transient gaps, according to criterion \eqref{eq:overlapcriterion}. The ratio of the couplings was set arbitrarily to $\rho = 0.5$.}
		\label{fig:width}
\end{figure}

In the previous part, we considered a gap centered around energy $\overline{E}$ to be transient if its width $\Delta_l(\overline{E})$ vanished in the quasiperiodic limit:
\begin{equation}
\label{eq:limitcriterion}
	\Delta_l(\overline{E}) \xrightarrow[l \to \infty]{} 0.
\end{equation}
We introduce here a practical criterion to distinguish between transient and stable gaps.
Let $\overline{E}_l(n)$ be the mean energy inside the gap of label $n$, for the $l^\text{th}$ approximant.
Define the displacement of a gap as $D_l(n) = |\overline{E}_{l+1}(n) - \overline{E}_l(n)| $
We say the gap $n$ at step $l$ is transient if its half-width does not overlap significantly its counterpart at step $l+1$, ie if 
\begin{equation}
\label{eq:overlapcriterion}
	D_l(n) > \frac{a(\rho)}{2} ( \Delta_l(n) + \Delta_{l+1}(n) ),
\end{equation}
and stable otherwise.
We have introduced the \emph{overlap coefficient} $a(\rho)$, which quantifies the extent of overlap we require for a gap to be considered stable.
We require $a \leq 1$, since otherwise the gaps at step $l$ and $l+1$ do not overlap at all.
Numerically, we determine $a$ by requiring that any stable gap at step $l$ overlaps their counterpart at step $l+l'$ with $l'$ arbitrarily large.
As the ratio of the couplings $\rho$ is decreased, gaps are enlarged and overlap more and more.
Thus, the value of $a$ also decreases with $\rho$.
As an example, we find $a(0.5) = 0.5$ for the Fibonacci chain.

It is easy to see that criterion \eqref{eq:overlapcriterion}, applied to the Fibonacci chain, yields the same stable and transient gaps as criterion \eqref{eq:limitcriterion} 
It is not trivial that these two criteria are in general equivalent.
However, it does appear to be the case for all the quasiperiodic chains we tested numerically. 
We will therefore consider them equivalent from now.

\begin{figure}[htp]
	\centering
	\begin{subfigure}{0.45\textwidth}
		\centering
		\includegraphics[width=1.\textwidth]{img2/logloggapwidth_Fibonacci_l_16.pdf}
		\caption{\small{Fibonacci chain.}}
		\label{fig:logwidth1}
	\end{subfigure}
	%
	\begin{subfigure}{0.45\textwidth}
		\centering
		\includegraphics[width=1.\textwidth]{img2/logloggapwidth_sqrt3_l_12.pdf}
		\caption{\small{Chain $\irr = \sqrt{3}-1$.}}
		\label{fig:logwidth2}
	\end{subfigure}
	\caption{Gap width as a function of gap label in log-log scale, for the same parameters as in Fig.\ \eqref{fig:width}.}
		\label{fig:logwidth}
\end{figure}

Figures \eqref{fig:width1} \eqref{fig:width2} \eqref{fig:width3} show the widths of the gaps as a function of their label, for various quasiperiodic chains.
The global structure is similar in every case: the gap width is a globally decreasing function of the gap index (this is consistent with perturbation theory in the limit $\rho \to 1$, see \eg\ \cite{diffractionLuck}).
Here we also observe that width decreases as a power-law of the index, as shown on Fig.\ \eqref{fig:logwidth}.
In the case of the Fibonacci chain (Fig.\ \eqref{fig:logwidth1}), we notice log-periodic oscillations.
We make the hypothesis that these log-periodic oscillations, frequently observed in fractal systems, come from the discrete scale invariance of the Fibonacci Hamiltonian (Eq.\ \eqref{eq:recur_ham}).
See appendix \ref{app:sec:discrete_scale_invariance} for a more detailed discussion on the link between discrete scale invariance and log-periodic oscillations.
In the case of the $\irr = \sqrt{3}-1$ chain (Fig.\ \eqref{fig:logwidth2}), we observe oscillations but cannot identify them as log-periodic. We conjecture that this is related to the fact that for this chain, the Hamiltonian cannot be written in a discrete scale invariant way similar to that of Eq.\ \eqref{eq:recur_ham}.

Finally, we observe that regardless of the chosen irrational $\irr$, there is a critical label above which all gaps are transient, and below which all gaps are stable.

%Going to the next approximant does not significantly change the width and label of previously stable gaps.
%It is therefore impossible for stable gaps to have vanishing width, implying that they must satisfy the first stability criterion.
%Moreover, the transient gaps are always the gaps of largest index.
%Since the width is a decreasing function of the index, we conclude that their width vanishes. They thus also satisfy the first stability criterion.
%All numerical evidences are therefore in favor of the two criteria being equivalent.


\section{Conclusion and perspectives}

We have shown that the well known GLT is applicable not just to quasiperiodic chains but also to their approximants.
The price to pay is the introduction of \emph{transient gaps}, which vanish in the quasiperiodic limit. 
Fortunately, these gaps are easy to spot: they always have the highest gap label.

Moreover, we show that the gap label contains relevant physical information: it globally orders the gaps by decreasing width, and separates stable from transient gaps.
The gap label, which had \emph{a priori} no physical content, thus appears physically relevant.

A gap label is robust under deformation of the Hamiltonian, as long the gap does not close.
This indicates that it is of topological nature.
More precisely, when a defect is introduced in the chain, creating edge states, \cite{Levy2015} showed a direct correspondence between the gap labels and the winding number of the corresponding edge states.

Open problems include more finely describing the behavior of the gap width with the index and extending this description to higher dimensional quasicrystals, which are also known to have gaps \cite{Zijlstra, penrosegaps}.