\begin{chabstract}
In this chapter, we describe \emph{quasiperiodic tilings}, which we use to model quasicrystals.

We first introduce \emph{almost periodic functions}, and describe their fundamental properties: \emph{repetitivity, almost periodicity} and \emph{periodicity in higher dimension}. We next introduce quasiperiodic tilings and argue that they can be viewed as discrete counterparts of quasiperiodic functions.
In particular, we discuss how many quasiperiodic tilings can be seen as cuts of higher dimensional periodic lattices, via the \emph{cut-and-project method}. 
We show on a simple example that the diffraction figure such tilings exhibit \emph{Bragg peaks} -- signature of a long-range order--, a feature which is experimentally observed in quasicrystals.
We then examine in more details the properties of 1D quasiperiodic tilings, and argue that they are the aperiodic tilings which are \emph{the closest to being periodic}.
Finally, we introduce \emph{substitution methods} that can be used to build many quasiperidic tilings.
\end{chabstract}

\chapter{Building quasicrystals}
\label{chap:bulding_qc}

\minitoc

\section{Almost periodic functions}
An almost periodic function is a superposition of harmonics:
\begin{equation}
	f(x) = \sum_n c_n e^{ik_n x}.
\end{equation}
In particular, periodic functions like $\cos(x)$ are almost periodic.
Almost periodic functions also include aperiodic functions like
\begin{equation}
\label{eq:almost_periodic}
	f(x) = \cos(x) + \cos(\tau^{-1} x),
\end{equation}
where $\tau = (1+\sqrt{5})/2$ is the golden ratio. The choice of the golden ratio is arbitrary: replacing it by any other irrational number would also cause the function $f$ to be almost periodic.

\begin{figure}[htp]
\centering
\begin{subfigure}[b]{0.45\textwidth}
	\includegraphics[width=1.\textwidth]{img0/periodic_function_cut.pdf}
    \caption{}
    \label{fig:almost_periodic}
\end{subfigure}
%
\begin{subfigure}[b]{0.45\textwidth}
	\includegraphics[width=\textwidth]{img0/periodic_function_cut_shift.pdf}
    \caption{}
    \label{fig:almost_periodic_shift}
\end{subfigure}
\caption{(a): The almost periodic function \eqref{eq:almost_periodic}. (b): The almost periodic function (in red) superimposed with its translate (in blue).}
\end{figure}
\subsection{Repetitivity}
Fig.~\eqref{fig:almost_periodic} shows a plot of this function, which indeed looks ``almost'' periodic: the patterns of the graph of the function almost repeats themselves everywhere: almost periodic functions are highly \emph{repetitive}
Almost periodic functions have the property that they almost overlap with copies of themselves translated by appropriately vectors, as shown by Fig.\ \eqref{fig:almost_periodic_shift}.
More precisely, one can find $\epsilon$-almost periods $x_\epsilon$ such that
\begin{equation}
	|f(x-x_\epsilon) - f(x)| \leq \epsilon
\end{equation}
for $\epsilon$ as small as we want. For the almost period $x_\epsilon = 50$ shown on Fig.\ \eqref{fig:almost_periodic_shift}, we have $\epsilon \simeq 0.26$.

\subsection{Almost periodicity as periodicity in higher dimension}
\begin{figure}[htp]
\centering
\includegraphics[width=0.4\textwidth]{img0/periodic_function.pdf}
\caption{The almost periodic function \eqref{eq:almost_periodic} can be seen as a cut (in red) of the 2D periodic function \eqref{eq:periodic}.}
\label{fig:periodic_function}
\end{figure}
Consider the periodic function
\begin{equation}
\label{eq:periodic}
	\tilde f(x,y) = \cos(x) + \cos(y)
\end{equation}
whose contour plot is shown on Fig.\ \eqref{fig:periodic_function}.
The almost periodic function introduced earlier (Eq.\ \eqref{eq:almost_periodic}) can be expressed as
\begin{equation}
	f(x) = \tilde f(x, \tau^{-1}x)
\end{equation}
Thus, our almost periodic function $f$ can be seen as a cut of the 2D periodic function $\tilde f$ along the line $y = \tau^{-1}x$, as shown on Fig.~\eqref{fig:periodic_function}. 
In general \emph{almost periodic functions can be seen as cuts of periodic functions in higher dimension}. 
This property also holds for quasiperiodic tilings, which can be seen as cuts of higher dimensional periodic tilings.

\section{Quasiperiodic tilings}

A tiling is an arrangement of non-overlapping tiles covering the plane.
It can be periodic, or aperiodic. Quasiperiodic tilings are a specific class of aperiodic structures which can be viewed as discrete counterparts of almost periodic functions.
Throught this section, we take the example of the quasiperiodic \AB\ tiling \cite{ammann1992aperiodic}, involving lozenges and squares, as shown on the left part of Fig.\ \eqref{fig:tiling_and_plot}.

Such tilings are interesting for physicists as they model quasicrystals. 
The right part of Fig.\ \eqref{fig:tiling_and_plot} shows such a model: atoms are placed at the apexes of the tiles, and the edges of the tiles represent chemical bonds between the atoms. 
In the remainder of this thesis, we will indifferently call ``tiling'' a covering of the plane by tiles (left part of Fig.\ \eqref{fig:tiling_and_plot}) or the corresponding model of atoms (right part of Fig.\ \eqref{fig:tiling_and_plot}).

\begin{figure}[htp]
\centering
\begin{subfigure}[b]{0.45\textwidth}
	\centering
	\includegraphics[width=1.\textwidth]{img0/AB_tiling_and_plot.pdf}
    \caption{}
    \label{fig:tiling_and_plot}
\end{subfigure}
%
\begin{subfigure}[b]{0.45\textwidth}
	\centering
	\includegraphics[width=.6\textwidth]{img0/AB_inflation.pdf}
    \caption{}
    \label{fig:AB_inflation}
\end{subfigure}
\caption{(a): Left: a piece of the quasiperiodic \AB\ tiling. Right: the corresponding model of atoms. Lengths are expressed in units of the nearest neighbors distance. (b): The inflation rules used to produce the quasiperiodic \AB\ tiling (see text for the details).}
\end{figure}

The \AB\ tiling, like many other quasiperiodic tilings, can be produced by the \emph{substitution (or inflation) method}: 
\begin{enumerate}
	\item Start with a given tile or arrangement of tiles
	\item Replace every tile in the arrangement by a set of tiles, following a predetermined set of rules (see \eg\ Fig.\ \eqref{fig:AB_inflation})
	\item Repeat step 2.
\end{enumerate}
The substitution method is discussed in more details in Sec.\ \ref{sec:substitutions}.
For the moment let us just use it to prove that the \AB\ tiling is indeed aperiodic.
As seen on Fig.\ \eqref{fig:AB_inflation} substitution replaces a rhombus by 3 rhombuses and 2 squares, while it replaces a square by 4 rhombuses and 3 squares. Therefore, if the number of rhombuses and squares is respectively $N_R$ and $N_S$ in a given tiled region, then the number of rhombuses and squares in the inflated region is
\begin{equation}
	\begin{pmatrix}
	N_R' \\
	N_S' \\
	\end{pmatrix}
	=
    \begin{bmatrix}
		3 & 4 \\
		2 & 3
	\end{bmatrix}
	\begin{pmatrix}
	N_R \\
	N_S \\
	\end{pmatrix}.
\end{equation}
The matrix introduced here is called the \emph{substitution matrix} (or inflation matrix) of the tiling.
To create larger and larger patches of the \AB\ tiling, one can applies the substitution rule repetitively to an initial tiled region.
After an asymptotically large number of substitutions, the number of rhombuses per square on the patch is given by the ratio of the two components of the eigenvector of the substitution matrix associated with the largest eigenvalue. One finds here $N_R/N_S \sim \sqrt{2}$. Therefore, there are $\sqrt{2}$ rhombuses per square on the \AB\ tiling. This fraction is irrational, thus proving that the \AB\ tiling is aperiodic.


\subsection{Repetitivity}
\begin{figure}[htp]
\centering
\includegraphics[width=0.7\textwidth]{img0/AB_shift.pdf}
\caption{A piece of the quasiperiodic \AB\ tiling (blue) superimposed with a translate of itself (red). The two pieces ``almost'' perfectly superimpose, differing only along lines.}
\label{fig:AB_shift}
\end{figure}
Quasiperiodic tilings share with almost periodic functions the \emph{repetitivity} property: every pattern (arrangement of tiles) present in the tilings repeats, as can be seen on Fig.\ \eqref{fig:tiling_and_plot}.
Furthermore, quasiperiodic tilings are \emph{almost periodic}, in exactly the same sense as quasiperiodic functions: a copy of the tiling translated by a carefully chosen vector overlaps almost everywhere with itself.
Fig.\ \eqref{fig:AB_shift} illustrates this property for the \AB\ tiling.


\subsection{The \cp\ method}
\label{sec:cp}
\begin{figure}[htp]
\centering
\includegraphics[width=0.6\textwidth]{img0/AB_lift.pdf}
\caption{A portion of the \AB\ tiling, with each edge colored differently according to its orientation. Nodes are labeled with their 4D coordinates (see text for the details).}
\label{fig:lift}
\end{figure}
Fig.\ \eqref{fig:lift} displays a portion of the \AB\ tiling, with each edge colored differently according to its orientation. Remark that only 4 orientations are present in the tiling. 
One can associate to each of these 4 orientations one of the 4 unit vectors of $\mathbb{R}^4$.
Let us for example choose the mapping:
\begin{equation}
	\begin{aligned}
		(1,0,0,0) \leftrightarrow & \text{ horizontal edges (in green)} \\
		(0,1,0,0) \leftrightarrow & \text{ $\pi/4$ inclined edges (yellow)} \\
		(0,0,1,0) \leftrightarrow & \text{ vertical edges (red)} \\
		(0,0,0,1) \leftrightarrow & \text{ $3\pi/4$ inclined edges (blue)}
	\end{aligned}
\end{equation}
Mapping all the edges, the whole tiling is lifted to a 4D ``staircase-like'' surface. 
In particular, every node of the tiling corresponds to a point of integer coordinates in 4D, as shown on Fig.~\eqref{fig:lift}).
Said differently, the points of the quasiperiodic \AB\ tiling can be interpreted as projections of points of $\zahl^4$, the periodic square tiling in 4D.

Just like almost periodic functions, many quasiperiodic tilings -- including the \AB\ tiling -- have the property of being interpreted as cuts of \emph{periodic} lattices when lifted to a higher dimensional space.

One can construct quasiperiodic tilings the other way around, using the so-called cut-and-project (\cp{}) method.
We restrict ourselves to what is often called the ``canonical'' \cp\ method, which we describe now:
\begin{enumerate}
	\item Choose a higher dimensional space $E$ of dimension $D$, hosting a $D$-dimensional hypercubic lattice.
	\item Choose a $d$-dimensional subspace $E_\parallel \subset E$, which will host the quasiperiodic tiling. We shall often loosely refer to the $E_\parallel$ subspace as the \emph{slope} of the tiling.
	\item Select the lattice points falling in the infinite ``slice'' obtained by translating the unit cell of the lattice along $E_\parallel$,
	\item project these points to $E_\parallel$.
\end{enumerate}
It is not our purpose here to describe the \cp\ method in its full generality; the interested reader can find a more detailed exposition in the articles \cite{KaluginCP, DuneauCP, ElserCP} or in the book \cite{Baake2013}.

\begin{figure}[htp]
\centering
\includegraphics[width=0.8\textwidth]{img0/cut_and_project.pdf}
\caption{The cut and project method used to construct the Fibonacci quasiperiodic chain.}
\label{fig:cut_and_project}
\end{figure}
Fig.\ \eqref{fig:cut_and_project} shows the canonical \cp\ method in the case $D = 2$, $d = 1$.
In that case, the higher dimension lattice is a 2D cubic lattice. 
The unit cell (shown in bold on the figure) is translated along the line $E_\parallel$, of slope $\irr = \tau^{-1}$ where $\tau$ is the golden ratio. The lattice points falling in the so produced slice are shown in blue. 
They are then projected onto $E_\parallel$, yielding a chain of points called the \emph{Fibonacci chain}.

Along the Fibonacci chain, nearest neighbors are either separated by a ``large'' or a ``short'' distance, represented respectively by single and double lines on Fig.\ \eqref{fig:cut_and_project}, and corresponding respectively to projections of horizontal or vertical edges of the square lattice.
If the slope $\irr$ had been rational, say $\alpha = p/q$, the line $E_\parallel$ would have intercepted the lattice point of coordinates $(q, p)$. Therefore, the chain on $E_\parallel$ would have been \emph{periodic}, of period $p+q$ since the slice selects $q$ horizontal and $p$ vertical edges going from the origin to the point $(q,p)$.
In the case of the Fibonacci chain, the slope is irrational, $\irr = \tau^{-1}$. Therefore, $E_\parallel$ intercepts no lattice point except the origin. Thus, the chain is \emph{aperiodic}, and in fact \emph{quasiperiodic}, as we shall see later.
In this thesis, a quasiperiodic chain of irrational slope $\irr$ produced in that way is referred to as a canonical \cp\ chain, or as a Sturmian chain. This last denomination is justified in Sec.\ \ref{sec:qp_chains}.

\begin{table}[htp]
\centering
  \begin{tabular}{ l  c  c }
      \textbf{Quasiperiodic tiling} & \textbf{D} & \textbf{d} \\ \hline
      canonical \cp\ chains & 2 & 1 \\
      \AB\ & 4 & 2 \\
      Penrose & 5 & 2 \\ 
  \end{tabular}
  \caption{The different quasiperiodic tilings studied in the thesis.}
   \label{tab:tilings}
\end{table}
Table \eqref{tab:tilings} presents the different \cp\ tilings that will focus on here. Canonical \cp\ chains (and in particular the Fibonacci chain) will be studied in Chap.\ \ref{chap:qp_chains}, \ref{chap:gap_labeling} and \ref{chap:2d} while the two-dimensional \AB\ and Penrose tilings will be studied in Chap.~\ref{chap:2d}.

\subsection{Diffraction pattern of a \cp\ tiling.}
For the moment, we define a quasiperiodic tiling as an aperiodic tiling with some kind of long range order, as evidenced by the fact that the tiling almost overlaps with a copy of itself translated.
From a physical point of view, performing a diffraction experiment can reveal the long range order of a structure.
Based on this observation, we introduce the following definition of a quasicrystal.
\begin{definition}[Quasicrystal]
A quasicrystal is a material which is both aperiodic -- in at least one direction of space -- and long range ordered. The latter is manifested by the occurrence of sharp spots in its diffraction pattern.
\end{definition}
In this part we compute the diffraction pattern of the \cp\ chains introduced earlier, and show that it consists of Bragg peaks. This motivates the use \cp\ tilings to model quasicrystals.
This remark was made very early in the development of quasicrystals theory, see \cite{KaluginCP, DuneauCP, Levine86, ElserCP}.

\begin{figure}[htp]
\centering
\includegraphics[width=0.5\textwidth]{img0/diffraction_setup.pdf}
\caption{Schematic setup of the diffraction experiment of a chain of atoms: the incident wave has wavevector $\mathbf{k}$, and $\mathbf{k}'$ is the wavector of the wave emitted by atom $m$ towards the observation point $\mathbf{R}$. $\mathbf{K} = \mathbf{k}' - \mathbf{k}$ is the scattering vector.}
\label{fig:diffraction_setup}
\end{figure}
We shine light on a chain of atoms, modeled by point-like particles perfectly reflecting light.
According to Huygens–Fresnel principle, and disregarding light interference within the sample, the amplitude diffracted at point $\mathbf{R}$ by the $m^\text{th}$ atom, located at point $\mathbf{r}(m)$, is
\begin{equation}
	\psi(\mathbf{R}) = A_0 e^{-i(\mathbf{k} \cdot \mathbf{r}(m) + \mathbf{k}'\cdot(\mathbf{R}-\mathbf{r}(m))},
\end{equation}
where $A_0 = \sqrt{I_0/N}$, with $I_0$ the total intensity of the incident beam.
Introducing the scattering vector $\mathbf{K} = \mathbf{k}'-\mathbf{k}$, as shown on Fig.~\eqref{fig:diffraction_setup}, one gets
\begin{equation}
	\psi(\mathbf{R}) = A_0 e^{-i \mathbf{k}'\cdot\mathbf{R}}e^{i\mathbf{K} \cdot \mathbf{r}(m)}
\end{equation}
%The total intensity is the modulus squared of sum of the amplitudes diffracted by the atoms.
%The total intensity diffracted by a region of $N$ atoms of the chain is therefore:
%\begin{equation}
%	I(\mathbf{K}) \simlim{|\mathbf{R}|\to \infty} \frac{I_0}{N} \Big| \sum_{j=1}^N e^{i\mathbf{K} \cdot \mathbf{r}(j)} \Big|^2
%\end{equation}
%where $I_0$ is the incoming intensity on the chain.
Let us write $\mathbf{K} \cdot \mathbf{r}(j) = K r(j) \cos \vartheta = k_\theta r(j)$, with $k_\vartheta = K \cos \vartheta$.
We are interested the total diffracted amplitudes, which writes\footnote{dropping the constant prefactor $A_0 e^{-i \mathbf{k}'\cdot\mathbf{R}}$}
\begin{equation}
	A(\vartheta) = \sum_{j} e^{i k_\vartheta r(j)}.
\end{equation}

This sum is a priori not easy to evaluate since the function $r(j)$ giving the position of the atom is non-trivial for a quasiperiodic chain, as can be seen on Fig.\ \eqref{fig:cut_and_project}.
However, using the \cp\ construction, we can instead sum over the points of the square lattice falling in the band:
\begin{equation}
	A(\vartheta) = \sum_{\mathbf{r} \in \text{band}} e^{i k_\vartheta r_\parallel(m,n)}
\end{equation}
where $r_\parallel$ is the parallel coordinate of the square lattice point $\mathbf{r}$.
We recall that if the slope of the \cp\ tiling is $\irr = \tan \theta$, then the parallel and perpendicular coordinates of the square lattice point $\mathbf{r} = (m,n)$ are
\begin{equation}
	\begin{pmatrix}
		r_\parallel\\
		r_\perp\\
	\end{pmatrix}
	=
	\begin{bmatrix}
	\cos \theta & \sin \theta\\
	-\sin \theta & \cos \theta\\
	\end{bmatrix}
	\begin{pmatrix}
	m\\
	n\\
\end{pmatrix}
\end{equation}
Restricting the sum to the band complicates the computation. Let us instead sum over all lattice points and weight the sum by the function $\mathds{1}_W(r_\perp)$ that takes value 1 if point $\mathbf{r}$ is inside the band, and 0 otherwise:
\begin{equation}
	A(\vartheta) = \sum_{\mathbf{r} \in \zahl^2} e^{i k_\vartheta r_\parallel} \mathds{1}_W(r_\perp)
\end{equation}
Explicitly,
\begin{equation}
	\mathds{1}_W(r_\perp) = \begin{dcases*}
					1 & if $0 \leq r_\perp < W$ \\
					0 & otherwise
				\end{dcases*}
\end{equation}
where $W$ is the width of the band: $W = \cos \theta + \sin \theta$.
Performing sum over a lattice is made easy by the \emph{Poisson summation formula} relating the sum of a function over a lattice to the sum of its Fourier transform:
\begin{equation}
	\sum_{\mathbf{r} \in \zahl^2} f(\mathbf{r}) = \sum_{\boldsymbol{p} \in 2\pi\zahl^2} \hat f(\boldsymbol{p})
\end{equation}
Applying the Poisson summation formula to our case, and performing the Fourier transform, one gets
\begin{equation}
	A(\vartheta) = 2 \pi W \sum_{\boldsymbol{p} \in 2\pi\zahl^2} \delta(p_\parallel - k_\vartheta) e^{-i W p_\perp/2} \sinc\left(\frac{W p_\perp}{2}\right)
\end{equation}
Now, the diffracted intensity being the modulus squared of the diffracted amplitude: $I(\vartheta) = \lim_{N \to \infty} I_0 |A(\vartheta)|^2/N$, we get
\begin{equation}
\boxed{
	I(\vartheta) \propto I_0 \sum_{\boldsymbol{p} \in 2\pi\zahl^2} \delta(p_\parallel - k_\vartheta)\sinc^2\left(\frac{W p_\perp}{2}\right) %((2\pi)^3 W)^2
	}
\end{equation}
So, the diffraction spectrum of any canonical \cp\ chain consists purely of Bragg peaks, as announced.
It is clear from this simple calculation that the origin of the Bragg peak structure is the underlying higher dimensional lattice. A similar calculation would yield a Bragg peak diffraction spectrum for any other \cp\ tiling.

Let us consider the particular case $\irr = 0$. In that case the chain is not quasiperiodic, but periodic. Bragg peaks are located at Fourier space positions $p_\parallel  = 2\pi m$ with $m$ integer.
Therefore, the Bragg peaks are indexed by a single integer, and their set forms a lattice. 

Let us now consider the case of a quasiperiodic chain, \ie\ the case $\irr =\tan \theta$ irrational.
Let us consider for example the case $\irr = 1/\sqrt{2}$. Then Bragg peaks are located at Fourier space positions $p_\parallel  \propto \sqrt{2} m + n$, with $m$ and $n$ integers.
Therefore, the Bragg peaks are indexed by two integers, and their set is not a lattice, but a dense set.

These results are generic, and are summed up in Tab.\ \eqref{tab:diffraction}.
\begin{table}[htp]
\centering
  \begin{tabular}{ c c c }
       & \textbf{Periodic} & \textbf{Quasiperiodic} \\ \hline
      \textbf{Diffraction spectrum} & Bragg peaks & Bragg peaks \\
      \textbf{Set of Bragg peaks} & discrete (lattice) & dense \\
      \textbf{Indexed by} & $d$ integers & $D > d$ integers \\ 
  \end{tabular}
  \caption{Properties of the diffraction spectrum for $d$ dimensional periodic and quasiperiodic sets of atoms.}
   \label{tab:diffraction}
\end{table}

%These results are generic: \emph{for a quasiperiodic tiling, the Bragg peaks are indexed by a number of integers $D > d$ which corresponds to the dimension of the higher dimensional space used by the \cp{}, and form a dense set.}

\section{Quasiperiodic chains}
\label{sec:qp_chains}
Quasiperiodic tilings are somehow in between periodic and disordered tilings. Are they closer to periodic or to disordered tilings?
In this section, we try to give an answer to this yet ill-posed question, in the context of \cp\ chains introduced earlier.

\subsection{Words and complexity}
\label{sec:complexity}
\begin{figure}[htp]
\centering
\input{img0/chain_and_word}
\caption{A piece of the Fibonacci chain and the corresponding word.}
\label{fig:chain_and_word}
\end{figure}
As we have seen before (see Fig.\ \eqref{fig:cut_and_project}), canonical \cp\ chains involve two distances, that are shown as single and double bonds on the figures.
Let us associate to the single bond (large distance) the letter $\A$ and to the double bond the letter $\B$, as shown on Fig.\ \eqref{fig:chain_and_word}.
To every chain of two lengths we can associate a word of two letters, and vice-versa.

Let $\word$ be an infinite word, \ie\ an infinite succession of letters $\A$ and $\B$. We are actually going to work mainly with semi-infinite words here, but results derived here can easily be extended to infinite words, as done for example in \cite{Pytheas}. 
A \emph{factor of length $N$} of $\word$ is a succession of $N$ contiguous letters appearing in the word. For example, $\lett{AB}$ is a factor of length 2 of the word $\word = \lett{BABAA}\dots$, as it appears after the first letter of $\word$.
We define the \emph{complexity} $\cmplx{\word}{N}$ as the number of distinct factors of length $N$ appearing in $\word$.

Obviously the complexity function cannot grow faster than $2^N$ but how slowly can it grow?
To answer the question, we consider an infinite (or semi-infinite) word, and we construct the \emph{tree of words} \cite{treeofwords_original, treeofwords}: a tree graph constructed as follows.
Vertices at level $N$ list the factors of length $N$. A factor $v$ of the level $N-1$ shares an edge with a factor $w$ of the level $N$ if $w$ can be constructed by adding one letter to the right of $v$.
For example, consider, the periodic word $\word$ of period $\lett{AB}$: $\word = \lett{ABAB}\dots$.
Its tree of words is shown on Fig.\ \eqref{fig:periodic_tree}.
\begin{figure}[htp]
\centering
\input{img0/periodic_tree}
\caption{The first levels of the tree of words of the periodic word $\lett{ABAB}\dots$.}
\label{fig:periodic_tree}
\end{figure}

Remark that the tree of this periodic word does not grow after level $N = 1$ ($N=0$ is the root of the tree). In general, \emph{a word is periodic of period $N$ if and only if its tree stops growing at level $N-1$}.
Indeed, suppose that at level $N-1$ on the tree of a given word $\word$, each vertex has only one child. Then there is only one way of adding a letter to the right of any factor of length $N-1$.
But then, there is again only one way to add a letter to the right of a subword of the length $N$, and by recursion the tree stops growing starting from level $N$. Thus, $\word$ is periodic of period $N$.
We conclude: \emph{A word $\om$ is aperiodic if and only if its complexity function grows at least linearly. More precisely,}
\begin{equation}
\boxed{
	\cmplx{\text{aperiodic word}}{N} \geq N+1
	}
\end{equation}
This is the Morse–Hedlund theorem, which is commented in more details in \cite{Pytheas}.

\subsection{Sturmian words}
\begin{figure}[htp]
\centering
\input{img0/fibonacci_tree}
\caption{The first levels of the Fibonacci tree of words.}
\label{fig:fibonacci_tree}
\end{figure}

Words whose complexity grows the slowest (\ie\ linearly) -- and which are in this sense the closest to being periodic -- are called \emph{Sturmian words}.

The Fibonacci word is an example of Sturmian word. 
Fig.\ \eqref{fig:fibonacci_tree} shows the first levels of the tree of the Fibonacci word, and one can indeed check that the tree width grows linearly.

It is easy to understand that \emph{all \cp\ words are Sturmian}.
Take an irrational number, and consider the word produced by the \cp\ method. 
We set as shown on Fig.\ \eqref{fig:cut_and_project} the origin of our chain at the (unique) point where the slope $E_\parallel$ intersects $\zahl^2$.
To enumerate the possible factors of length $N$, we translate the slope along its normal direction, and consider the set of words of length $N$ that appear on the right of the origin.
This set is exactly the set of factors of length $N$, because translating the slope while keeping the origin fixed amounts to translating the origin while keeping the slope fixed.
When we translate the slope, we pass from one word to the next by what is called a \emph{phason flip}. 
Two phasons flips cannot occur at the same time, since the slope is irrational.
Moreover, after having translated the slope along one unit cube of $\zahl^2$, we are back to the original setup, and in the interval of $N$ letters we consider, $N+1$ phason flips have occurred, yielding at most $N+1$ different words of length $N$. Thus the complexity is at most $N+1$. It cannot be less, because the word is aperiodic. We conclude it is exactly $N+1$.

In fact it turns out that \emph{the Sturmian words are exactly the \cp\ words.}, as proved in Chap.\ 6 of \cite{Pytheas}.
This result corroborates our intuition that \cp\ tilings are the least complex among aperiodic tilings, and in this sense, the closest to being periodic.

\section{Substitutions}
\label{sec:substitutions}
In this last section, we describe in more details the \AB\ substitution rule introduced earlier, after briefly showing how some Sturmian chain can be constructed by substitution.
\subsection{1D substitutions}
\subsubsection{Constructing the Fibonacci chain by substitution}
Consider the following \emph{substitution rule}:
\begin{equation}
	\sub: 
	\begin{cases}
        \A \rightarrow \lett{AB} \\
        \B \rightarrow \A
    \end{cases}
\end{equation}
Starting with a finite word $\word_0 = \B$, we can apply the substitution rule to obtain a series of longer and longer chains : $\word_1 = \sub(C_0) = \B$, $\word_2 = \sub(C_1) = \lett{AB}$, \dots $\word_l = \sub^{l}(C_0)$.
The semi-infinite chain $\word_\infty$ coincides with the \cp\ chain obtain by keeping on Fig.\ \eqref{fig:cut_and_project} the part of the chain to the right of the projection of point $(0,1)$.
The (bi)infinite Fibonacci chain can also be produced by applying the same substitution on the initial word $\word'_0 = \B|\A$\footnote{The bar marks the origin, corresponding to the point $(0,1)$ on Fig.\ \eqref{fig:cut_and_project}}: $\word'_1 = \sub(\B)|\sub(\A) = \A|\lett{AB}$, $\word'_2 = \sub(\A)|\sub(\lett{AB}) = \lett{AB}|\lett{ABA}$, \dots
We shall indifferently refer to $\word_\infty$ and to $\word'_\infty$ as ``the'' Fibonacci chain, disregarding the differences between infinite and semi-infinite chains. For a more detailed discussion, the reader is referred to Chap.\ 4 of \cite{Baake2013}.

\subsubsection{Substitution and scale invariance}
In the \cp\ picture, A and B letters are represented by long and small segments, whose ratio of lengths $L_\B/L_\A$ is equal to the golden ratio $\tau = (1+\sqrt{5})/2$.
Therefore, from a geometrical point of view, the substitution transforms a small segment of length $L_B$ into a segment whose length $L_\A$ is $\tau$ times larger.
Similarly, it transforms a large segment of length $L_\A$ into a segment whose length $L_\A + L_\B$ is again $\tau$ times larger.
Therefore, the substitutions rescales the lengths by a factor of $\tau$.
Since the infinite Fibonacci chain is by construction invariant under the substitution, it has a scale invariance property. This scale invariance property will be exploited in Chap.\ \ref{chap:qp_chains} to explain the multifractal nature of the spectrum and eigenstates of a quantum model built on the Fibonacci chain.

\subsubsection{Constructing Sturmian chains by substitution}
We have seen that the Fibonacci chain can be constructed by substitution.
Is this true for every canonical \cp\ chain? The following theorem \cite{sturmiansub} answers in the negative.
\begin{theorem}
A Sturmian word can be obtained by substitution if and only if its slope $\irr$ is a quadratic irrational and if its intercept $\rho$ is in the field extension of $\irr$: $\rho = m + n \irr$, with $m$ and $n$ rational numbers.
\end{theorem}
We recall that a quadratic irrational is an irrational root of a quadratic polynomial with integer coefficients. We easily check that the Fibonacci chain verifies the conditions of the theorem. Indeed, its slope is the inverse of the golden ratio, $\omega$, which satisfies the quadratic equation $\omega^2 = 1 - \omega$, and its intercept is $\rho = 0$.

The converse is also false: not all substitutions correspond to a canonical \cp\ chain. In Chap.\ \ref{chap:2d} we shall study such a non-\cp\ substitution.

\subsection{2D substitutions: the example of the \AB\ tiling}

In this thesis we study two 2D quasiperiodic tilings, the Penrose and \AB\ tilings. As we already mentioned, both of them can be constructed by the canonical \cp\ method. Alternatively, they can also be built using a substitution rule. We only discuss here the \AB\ substitution, but the principle is the same in the case of the Penrose tiling.

\begin{figure}[htp]
\centering
\includegraphics[width=0.5\textwidth]{img0/substitution_AB.pdf}
\caption{A reformulation of the \AB\ substitution rule.}
\label{fig:substitution2_AB}
\end{figure}

The substitution rule for the \AB\ tiling was already shown on Fig.\ \eqref{fig:AB_inflation}. In order to alleviate the notations, we will now operate on rhombuses and half-squares (also known as triangles) instead of working on the whole squares.
The substitution rule is then recast as shown on Fig.\ \eqref{fig:substitution2_AB}.
Notice that there are two non-equivalent ways to inflate a half-square, both of which are shown on the figure.
For a given half-square inserted in a tiling patch, only one of the two possible inflations is compatible with the local arrangement of the tiles. In order to conveniently book-keep which inflation to use, arrows are drawn on the edges of the tiles.
These arrows will be given a physical meaning in Chap.\ \eqref{chap:2d}: as we shall see, they are involved in the structure of some electronic eigenstates.

In view of modeling quasicrystals, we are no so much interested in a tile model (left panel of Fig.\ \eqref{fig:AB_patch}) as in a model of atoms linked to neighboring atoms (right panel of Fig.\ \eqref{fig:AB_patch}).
Therefore, we are interested in knowing how the substitution rule acts on the atomic positions, shown in Fig.\ \eqref{fig:substitution2_AB} as dots.
As presented on Fig.\ \eqref{fig:lift}, we can label atomic positions with 4D vectors of integers.
In the \cp\ formalism, these vectors are the coordinates of the 4D lattice points which are then projected to $E_\parallel$ to form the tiling.
One can show that the 4D vector $\mathbf{v}$ labeling a given atom is transformed by substitution into the vector $M \mathbf{v}$ where $M$ is the matrix
\begin{equation}
	M
	=
	\begin{bmatrix}
	1 & 1 & 0 & -1\\
	1 & 1 & 1 & 0 \\
	0 & 1 & 1 & 1 \\
	-1 & 0 & 1 & 1
	\end{bmatrix}.
\end{equation}
The substitution also create new atoms.
Using the notation introduced in Fig.\ \eqref{fig:substitution2_AB}, the 4D vectors of the atoms created when inflating a rhombus are
\begin{equation}
	\begin{aligned}
		\vec{p}_1 & = (M - 1) \vec{a}_1 + \vec{b}_1 \\
		\vec{p}_2 & = (M - 1) \vec{a}_2 + \vec{b}_1 \\
		\vec{q}_1 & = (M - 1) \vec{a}_1 + \vec{b}_2 \\
		\vec{q}_2 & = (M - 1) \vec{a}_2 + \vec{b}_2 \\
		\vec{r}_1 & = \vec{p}_1 + \vec{q}_1 - M \vec{a}_1 \\
		\vec{r}_2 & = \vec{p}_2 + \vec{q}_2 - M \vec{a}_2
	\end{aligned}.
\end{equation}
Similarly, the 4D vectors of the atoms created when inflating a half-square are
\begin{equation}
	\begin{aligned}
		\vec{p} & = (M - 1) \vec{b} + \vec{a} \\
		\vec{q} & = \vec{r} - \vec{c} + \vec{b} \\
		\vec{r} & = (M(\vec{c} + \vec{b}) + \vec{c} - \vec{b})/2 \\
		\vec{s} & = (M - 1) \vec{a} + \vec{c} \\
		\vec{t} & = \vec{q} + \vec{a} - \vec{b}
	\end{aligned}.
\end{equation}

A python implementation of the \AB\ substitution as presented here can be found on the git repository of this thesis: \url{https://framagit.org/Yukee/PhD_thesis/blob/master/demo/Ammann-Beenker.ipynb}.

\section{Conclusion}

In this chapter, we have presented some of the numerous ways of building quasiperiodic tilings.
We have also exhibited some of the geometrical of these structures: repetitivity, complexity, diffraction patterns, inflation symmetry.
We are now ready to study the single electron problem on quasiperiodic structures in one and two dimensions.