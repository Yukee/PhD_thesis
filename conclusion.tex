\chapter{Conclusions}

We have studied the problem of simple non-interacting tight-binding Hamiltonians in quasiperiodic tilings.
It has been known for a long time that such Hamiltonians have uncommon properties: they behave like systems at a phase transition, with a multifractal spectrum and critical eigenstates.
Understanding how these properties stem from the quasiperiodic geometry is a task that was undertaken very early in the history of quasicrystals, and remains still very challenging.

~\\

During this thesis, we have examined first what is perhaps the most simple and well studied quasiperiodic system: the Fibonacci chain.
Early studies have shown that the multifractality of the eigenstates is linked to the quasiperiodic nature of the Fibonacci chain.
In particular, the perturbative renormalization group approach that we considered, valid in the strong modulation limit, explicitly relates the fractal nature of the system to its inflation symmetry.
We have extended this well-known renormalization group method to describe the eigenstates at next-to-leading order, at which multifractal properties are seen to emerge.

The renormalization group has also been used to study spectral gaps.
In particular, it yields recurrence relations between the values the IDOS can take inside spectral gaps of Fibonacci approximants.
We exploited these to reformulate the well-known gap labeling theorem.
These recurrence relations between IDOS values help us understand how the spectrum of periodic chains of increasingly large period converge to the spectrum of the quasiperiodic chain.
In particular, we learn that depending on their label, gaps of approximants can be ``transient'' or ``stable'', according to whether or not they disappear in the quasiperiodic limit.
We presented numerical hints that these results can be transposed without difficulties to any other canonical \cp\ chain.

Interestingly, although the renormalization group we have used is approximate, with it we could non-perturbatively describe one eigenstate of the Fibonacci chain: the state at the middle of the spectrum.
We have studied this state in detail, and shown that it can be written as a function of a geometrical height field.
The distribution of the heights obeys a Fokker-Plank-like equation, where the number of inflations plays the role of time. 
Solving this equation and finding the asymptotic form of the distribution of heights gives access to properties of the central state.
In particular, we have shown that the multifractal spectrum of the state, and the conductivity at the middle of the spectrum were directly related to the asymptotic form of the distribution of heights, which we could compute exactly.
The results presented here are not limited to the Fibonacci chain: any quasiperiodic chain admits a central state of the same type, with the same properties. 
A contrario, we have shown that aperiodic chains which are not quasiperiodic have a central state of a totally different nature: localized instead of being critical.

In two dimensions, previous works have identified eigenstates decribed by similar height fields.
Building on a recent ansatz for the ground state of two dimensional tilings, we showed that these are a solution for a family of Hamiltonians, and that they possess, just like their 1D counterparts, a multifractal spectrum that we can compute exactly.

~\\

I have only scratched the surface of this fascinating topic.
Finding robust states whose physical properties can be computed easily is exciting. However, these states are probably not typical in the vast set of eigenstates of a given model. For example, the ground state of the Fibonacci chain does not belong to their class, as we have shown... New ideas are still needed, and many things are still to be discovered!
Further directions can be decomposed into two classes: i) the same tools and methods to other geometrical models (and they are plenty!), and ii) building on the knowledge we have of previously studied models to tackle different problems: single particle transport, magnetism, interactions.
