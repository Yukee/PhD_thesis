\chapter{Introduction}

The structures the most studied in condensed matter physics are either crystalline (periodic), or amorphous (disordered). However, in this thesis we study quasicrystals, systems which neither crystalline nor amorphous, which are in some sense in between the two.
Before motivating this study on physical grounds, we briefly present quasiperiodicity, via a ``real life'' example taken from \cite{Bedaride2013}.

Having decided to re-tile your bathroom, you go to the closest department store, where two types of tiles are available: squares and a rhombuses (Fig.\ \eqref{fig:ab_tiles}). They are accompanied by the following intriguing offer: ``Pay cash for squares -- and get all the rhombuses you want for free!''.

\begin{figure}[H]
\centering
\begin{subfigure}[b]{0.45\textwidth}
	\centering
	\includegraphics[width=.9\textwidth]{imgIntro/tiles.pdf}
    \caption{}
    \label{fig:ab_tiles}
\end{subfigure}
%
\begin{subfigure}[b]{0.45\textwidth}
	\centering
	\includegraphics[width=.5\textwidth]{imgIntro/forbidden.pdf}
    \caption{}
    \label{fig:forbidden}
\end{subfigure}
\caption{(a): The two tiles available at the store. (b): A forbidden configuration.}
\end{figure}

Wanting to re-tile your bathroom in the cheapest possible way, you briefly think of taking only the free rhombuses, before realizing that because of the notches on the edges of the tiles, tiling with rhombuses only is not possible, as shown on Fig.\ \eqref{fig:forbidden}.
So, you need at least some squares to accommodate the rhombuses.
A tiling with squares only would be possible but that would of course be a loss of money.
What is the cheapest possible tiling, \ie{}, what is the largest possible fraction of free rhombuses on can introduce?
After thinking about it, you realize (see \cite{Bedaride2013}) that the cheapest tiling has $\sqrt{2} \simeq 1.4$ rhombus per square, and looks like this:
\begin{figure}[H]
\centering
\includegraphics[width=0.5\textwidth]{imgIntro/ammann.pdf}
\caption{A patch of the \AB\ tiling (notches were omitted).}
\end{figure}
This tiling, called the \emph{\AB\ tiling}, has curious properties.
It must be \emph{aperiodic} since the number of rhombuses per square is not rational, however, it looks \emph{almost} periodic, in the sense that it almost overlap with a copy of itself translated by a well-chosen vector, as seen on Fig.\ \eqref{fig:ab_overlap}.
The \AB\ tiling is an example of a \emph{quasiperiodic tiling}: an aperiodic yet long-range ordered structure.
\begin{figure}[htp]
\centering
\includegraphics[width=0.5\textwidth]{imgIntro/ammann-overlap.pdf}
\caption{\AB\ tiling (blue) almost overlapping with its translated copy (red).}
\label{fig:ab_overlap}
\end{figure}

It is part of the larger family of the \emph{aperiodic tilings}, whose origin can be traced back to 1961, when the logician Hao Wang and his student Robert Berger started working on the following question: \emph{Is the tiling problem decidable?}
In other words, is there an algorithm which, when provided with a set of tiles, returns ``yes'' or ``no'' depending on whether on can tile the plane with them or not.
Robert Berger proved in 1964 that no such algorithm can be written, and his proof involved the construction of a set of 20,426 tiles that can only be used to tile the plane aperiodically (see \cite{berger1966undecidability}). The first aperiodic tiling was born.

The Penrose tiling is perhaps the most famous example of aperiodic tiling. 
It is made of ``fat'' and ``thin'' rhombuses decorated with arrows, as shown on Fig.\ \eqref{fig:penrose_tiles}.
Two tiles can be adjacent if their arrows match on the edge they share.
Assembling them according to this rule necessarily produces the Penrose tiling, a patch of which is shown on Fig.\ \eqref{fig:penrose_patch}.
The tiling's edges have 10 possible orientations, and it turns out they all occur with the same probability: the Penrose tiling is thus 10-fold symmetric.
One can show that this symmetry is forbidden for a periodic structure (see \eg\ the introduction of \cite{Baake2013}). Therefore, by relaxing the periodicity constrain, one gains access to a broader class of symmetries.
Furthermore, just like the \AB\ tiling, the Penrose tiling is quasiperiodic and possesses almost periods.

\begin{figure}[htp]
\centering
\begin{subfigure}[b]{0.45\textwidth}
	\centering
	\includegraphics[width=1.\textwidth]{imgIntro/Penrose_notches.pdf}
    \caption{}
    \label{fig:penrose_tiles}
\end{subfigure}
%
\begin{subfigure}[b]{0.45\textwidth}
	\centering
	\includegraphics[width=\textwidth]{imgIntro/penrose_patch.pdf}
    \caption{}
    \label{fig:penrose_patch}
\end{subfigure}
\caption{(a): The two tiles of the Penrose tiling. (b): A patch of the Penrose tiling (arrows were omitted).}
\end{figure}

In the eighties, quasiperiodic tilings made their appearance in realm of physics.
In April 1982, Dan Shechtman was analyzing a series of rapidly solidified AlMn samples he had produced, when he noticed something strange: electron diffraction of some samples showed Bragg peaks -- signature of a long-range ordered structure -- together with a 10-fold symmetry -- incompatible with a periodic crystalline structure.
At that time, no structures apart from crystals were known to yield Bragg peaks, hence the initial puzzlement of Shechtman and of the physics community at large. 
Shechtman and his collaborators later understood (\cite{Shechtman1984}) that the atoms in such alloys organized in a quasiperiodic fashion, forming a structure similar to the Penrose tiling.
For his discovery, Shechtman was awarded the Wolf prize in 1999 and the Nobel prize in chemistry in 2011.
Experimentalists soon discovered new alloys showing crystallographically forbidden symmetries: a 12-fold symmetric NiCR structure \cite{Ishimasa1985}, another type of 10-fold symmetric AlMn structure \cite{Bendersky1985}, and 8-fold symmetric WNiSi and CrNiSi phases \cite{Wang1987}.
Such structures, organized in a quasiperiodic fashion, are called \emph{quasicrystals}.
Note that quasicrystals need not exhibit non-crystallographic symmetries \cite{Feng1990}.
Quasiperiodic structures are not restricted to intermetallic alloys: they also spontaneously form in soft matter systems, see \eg\ \cite{Iacovella2011} and references therein.
Finally, let us mention that quasiperiodic structures have been artificially engineered, see \eg\ \cite{light, tanese2014}.

What are the optical, acoustic, magnetic, electronic transport properties of quasicrystals? How do these properties vary with temperature, pressure, \dots\? What happens at the interface between a quasicrystal and another material?
All these questions, that are of interest when studying crystals, can be asked again in the case of quasicrystals.
Although 35 years have passed since their discovery, the questions listed above are still vastly unexplored from a theoretical point of view.
Indeed, with quasicrystalline structures being one the one hand too far from periodicity for Bloch's theorem to apply, and the other hand too correlated for the tools from disordered system to be relevant, theoreticians are deprived of their favorite tools for handling condensed matter problems.
But if quasicrystals are so hard to approach theoretically, this may be a good sign that the physics of these systems is new and interesting.

In this thesis, we study toy models for non-interacting electrons on simple 1D an 2D quasiperiodic tilings.
The interest for such models is not new, and is in fact \emph{anterior} to the experimental discovery of quasicrystals (see \cite{Kohmoto1983} and references therein).
In these systems one can find devil's staircase-like energy spectra, fractal eigenstates \dots strange structures seldom appearing in physically reasonable systems!
How does these unusual properties stem from quasiperiodicity?
This is the question we want to address in this thesis.

In Chap.\ \ref{chap:bulding_qc}, we present recipes for constructing quasiperiodic tilings, and we discuss their basic geometrical properties.
In particular, we argue that quasiperiodic tilings are, among aperiodic tilings, the closest to being periodic.

In Chap.\ \ref{chap:qp_chains}, we examine everyone's favorite example of 1D quasiperiodic system: the Fibonacci chain. With the help of a perturbative renormalization group, we explain how the fractality of the spectrum and eigenstates emerges from the scale-invariance properties of the chain. This chapter is based on \cite{fiboperturbMace}.

In Chap.\ \ref{chap:gap_labeling}, we discuss one the few exact results available for quasiperiodic chains: the gap labeling theorem, relating the integrated density of states' values to the geometry of the chain. We discuss in particular the gap labeling in finite chains, and the passage to the infinite size limit. We discuss how the labeling depends on the geometry of the chain by considering different examples. This chapter is based on \cite{mace2017gap}.

In Chap.\ \ref{chap:2d}, we present exact solutions for some eigenstates of hopping models on one and two dimensional quasiperiodic tilings and show that they are critical, fractal states, by explicitly computing their multifractal. 
These eigenstates are shown to be generically present in 1D quasiperiodic chains. We then describe properties of the ground states for a class of tight-binding Hamiltonians on the 2D Penrose and \AB\ tilings. This chapter is based on \cite{mace2017critical}
