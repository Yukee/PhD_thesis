\chapter{Properties of Sturmian sequences}
\label{app:sturmian_sequences}

\section{Counting letters}
Consider the Sturmian chain associated to $\irr$ (irrational or not).
Then, the number of A letters in the interval between site 0 and site $i$ is
\begin{equation}
	\nb{A}{\irr}(m) = \floor*{\frac{m}{1+\irr}}
\end{equation}
% reset the proof counter
\setcounter{proof}{0}
\begin{proof}[Counting letters]
\label{proof:counting_letters}
Let $\irr$ be the number used by canonical \cp\ to build the Sturmian word we consider.
We chose a point of the $\zahl^2$ lattice, and set the slope so that it passes through this point, which is chosen as the origin.
If now $\theta$ denotes the angle the slope makes with the horizontal axis (\ie\ $\irr = \tan \theta$), the point $\mathbf{x} = (i,j) \in \zahl^2$ has coordinates
\begin{align}
	x_\parallel & = i \cos \theta + j \sin \theta \\
	x_\perp & = j \cos \theta - i \sin \theta
\end{align}
in the basis whose first axis is along the slope, and whose second axis is such that the basis is direct and orthonormal.
Then, the condition for $\mathbf{x}$ to be in the tiling is $0 \leq x_\perp < \cos \theta + \sin \theta$, \ie\
\begin{equation}
	0 \leq \frac{m}{1+\irr} - i < 1,
\end{equation}
where we have introduced the integer $m = i + j$ which counts tiling points from left to right starting from the origin.
We recognize here the definition of the floor function, and we have
\begin{equation}
	i = \floor*{\frac{m}{1+\irr}}.
\end{equation}
Since $i$ counts the number of horizontal bounds we have encountered by going $m$ sites to the right from the origin, and since the number of horizontal bounds corresponds in the \cp\ picture to the number of A letters in the Sturmian word picture, the proof is complete.
\end{proof}
Similarly, the number of B letters is
\begin{equation}
\label{eq:counting_B}
	\nb{B}{\irr}{m} = m - \nb{A}{\irr}{m} = \ceil*{\frac{\alpha m}{1+\irr}}
\end{equation}

\section{Sturm functions}

Since $\nb{A}{\irr}(m)$ counts the number of A letters between sites 0 and $m$, the \emph{Sturm function}
\begin{equation}
	\sturm{\irr}{m} = \nb{A}{\irr}(m+1) - \nb{A}{\irr}(m)
\end{equation}
takes the values 0 or 1 according to
\begin{equation}
\label{eq:sturm_function}
	\sturm{\irr}{m} =
	\begin{cases}
		0 \text{~if B between $m$ and $m+1$,}\\
		1 \text{~if A between $m$ and $m+1$}.
	\end{cases}
\end{equation}
It thus generates the Sturmian sequence associated to $\irr$.\footnote{Mathematicians (see \eg\ \cite{Pytheas}) usually define the Sturm function by the difference of B letters, which means the role of 0 and 1 is exchanged in equation \eqref{eq:sturm_function}.}
Tab.~\eqref{tab:sturmian_sequence} shows the first few terms of the Fibonacci Sturmian sequence.
\begin{table}[htp]
\centering
\begin{tabular}{|c|ccccccccccccc|}
\hline
	$m$ &0 &1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 & 11 & 12 \\
\hline
	$\sturm{\tau^{-1}}{n}$ & 0 & 1 & 0 & 1 & 1 & 0 & 1 & 0 & 1 & 1 & 0 & 1 & 1\\
	sequence          & B & A & B & A & A & B & A & B & A & A & B & A & A\\
\hline
\end{tabular}
\caption{The first few terms of the Fibonacci sequence, associated with the inverse of the golden ration, $\tau^{-1}$. Below is shown the corresponding sequence of letters.}
\label{tab:sturmian_sequence}
\end{table}

\section{Mod 1 arithmetic and Sturmian sequences}
\label{app:sec:mod_1}
\subsection{Basic properties}
In this section we recall some results about numbers modulo 1, or equivalently, about fractional part of numbers.
Let us begin with equivalent definitions of the fractional part:
\begin{align}
	\fpart{x} & = x \mod 1, \\
	\fpart{x} & = x - \floor{x}, \\
	\fpart{x} & = x + 1 - \ceil{x}.
\end{align}
Algebraic properties:
\begin{align}
	\fpart{x+y} & = \fpart*{ \fpart{x} + \fpart{y} }, \\
	\fpart{xy} & = \fpart*{\fpart{x}\fpart{y}}.
\end{align}
A uselful one:
\begin{equation}
	\label{eq:fpartprop}
	\fpart{x} + \fpart{-x} = 1.
\end{equation}

\subsection{Mod 1 arithmetic and internal space coordinates}
We will frequently use fractional parts of the form
\begin{equation}
	\nperp{\irr}{n} = \fpart*{\frac{n}{1+\irr}}
\end{equation}
where $n$ is an integer and $\irr$ an irrational (quasiperiodic chain) or a rational (periodic chain) number.
It satisfies the following identities:
\begin{align}
	\nperp{1/\irr}{-n} & = \nperp{\irr}{n}, \\
	\nperp{\irr}{-n} & = 1 - \nperp{\irr}{n}.
\end{align}
Below, we show that $\nperp{\irr}{n}$ is the normalized perpendicular space position of $n$:
\begin{equation}
\label{eq:normalized_perp_pos}
	\nperp{\irr}{n} = \frac{x_\perp(\irr,n)}{H(\irr)}
\end{equation}
where $H(\irr) = (1+\irr)/\sqrt{1+\irr^2}$ is the width of the canonical window.
\begin{proof}[Normalized perpendicular position]
From the proof \eqref{proof:counting_letters} one knows that the perpendicular space projection of point $\mathbf{x} = (i,j)$ is 
\begin{equation}
	x_\perp = j \cos \theta - i \sin \theta.
\end{equation}
We have $\cos \theta = 1/\sqrt{1+\irr^2}$ and $\sin \theta = \irr/\sqrt{1+\irr^2}$, so that the window width is $H(\irr) = \cos \theta + \sin \theta = (1+\irr)/\sqrt{1+\irr^2}$.
Using the expressions of the $\cos$ and $\sin$, and then the expression of $i$ and $j$ derived in the proof \eqref{proof:counting_letters}, we get successively
\begin{align*}
	\sqrt{1+\irr^2} x_\perp & = j - \irr i, \\
	\sqrt{1+\irr^2} x_\perp & = \nb{B}{\irr}{m} - \alpha \nb{A}{\irr}{m}.
\end{align*}
where $m = i + j$.
Using the basic properties of the fractional part, we arrive at
\begin{align*}
	\sqrt{1+\irr^2} x_\perp & = 1 - \fpart*{\frac{\irr m}{1+\irr}} + \irr \fpart*{\frac{m}{1+\irr}}, \\
	\sqrt{1+\irr^2} x_\perp & = 1 - \fpart*{-\frac{m}{1+\irr}} + \irr \fpart*{\frac{m}{1+\irr}}, \\
	\sqrt{1+\irr^2} x_\perp & = (1+\irr)\fpart*{\frac{m}{1+\irr}}.
\end{align*}
\end{proof}
In perpendicular space, the tiles are arranged according to their local environment. In particular,
\begin{equation}
	\begin{cases}
		0 \leq \nperp{\irr}{m} < \frac{\irr}{1+\irr} \implies \text{~B between $m$ and $m+1$,} \\
		\frac{\irr}{1+\irr} \leq \nperp{\irr}{m} < 1 \implies \text{~A between $m$ and $m+1$}.
	\end{cases}
\end{equation}

\subsection{Approximants}
\label{app:sec:approximants}
In this section, we discuss how a given Sturmian word can be approximated by periodic words called \emph{approximants}.
\subsubsection{Best approximants of an irrational number}
Consider $\irr$ irrational. We want to construct a series of rational approximations of $\irr$, \ie\ rational numbers $\irr_l$ such that $\irr_l \to \irr$ when $l \to \infty$.

Consider the fraction $p/q$. 
Reducing the fraction if needed, we can consider $p$ and $q$ coprime.
In this case, the canonical \cp\ word of slope $p/q$ is periodic, with a unit cell of $L = p+q$ letters.
We say that $p/q$ is a \emph{best approximant} of $\irr$ if
\begin{equation}
	|q \irr - p| < |q' \irr - p'|
\end{equation}
for all integers $q' \leq q$ and for all integers $p'$. Note that in particular
\begin{equation}
	\Big| \irr - \frac{p}{q} \Big| < \Big| \irr - \frac{p'}{q'} \Big|
\end{equation}
so that among all fractions of denominator smaller or equal to $q$, best approximants best approximate $\irr$.

\subsubsection{Convergents and best approximants}
Let us now introduce a practical way of generating best approximants.
Consider the continued fraction representation of $\irr$, which writes, for $\irr < 1$ (which is the case we are interested in here) as:
\begin{equation}
	\irr = \frac{1}{a_1 + \frac{1}{a_2 + \dots}}
\end{equation}
also written as $\irr = [a_1, a_2, \dots]$. The positive integers $a_i$ are uniquely defined.
The l$^\text{th}$ \emph{convergent} of $\irr$ is the rational number obtained by cutting the continued fraction at position $l$: $\irr_l = [a_1,a_2,\dots a_l]$.
It turns out that \emph{convergents are best appproximants} of $\irr$.
We shall use them to produce periodic approximants of a given Sturmian word.
Successive convergents verify
\begin{equation}
\label{eq:convergents_modular_group}
	p_{l-1}q_{l} - p_{l}q_{l-1} = (-1)^l
\end{equation}

\subsubsection{Conumbering and best approximants}
In $\zahl^2$, the period of the $l^\text{th}$ approximant is $\mathbf{b}_l = (q_l, p_l)$.
Define now the \emph{generator} $\mathbf{a}_l = (q_l', p_l')$ such that $\mathbf{a}_l$ and $\mathbf{b}_l$ form a unit cell of the approximant:
\begin{equation}
	\mathbf{b}_l \times \mathbf{a}_l = 1
\end{equation}
Given equation \eqref{eq:convergents_modular_group}, the previous convergent is suited to build the generator: $\mathbf{a}_l = (-1)^l (q_{l-1}, p_{l-1})$.
For example, in the case of the Fibonacci chain, the l$^\text{th}$ convergent is $\irr_l = F_{l}/F_{l+1}$ (where $F_l$ is the l$^\text{th}$ Fibonacci number, $F_1 = F_2 = 1$), so that one has $\mathbf{b}_l = (F_{l+1}, F_{l})$ and $\mathbf{a}_l = (-1)^l(F_{l},F_{l-1})$.

We can write a given lattice point $\mathbf{n} = (i,j) \in \zahl^2$ in this new basis:
\begin{equation}
	\mathbf{n} = c \mathbf{a}_l + k \mathbf{b}_l
\end{equation}
The $c$ coefficient has a natural interpretation.
Indeed, noting that $m = i+j$ is the labeling of the m$^\text{th}$ point of the \cp\ tiling, then $c$ can be written as
\begin{equation}
\label{eq:conumbering}
	c = m q_l \mod p_l + q_l
\end{equation}
We shall call say that $c$ is the \emph{conumbering of $m$}.
The conumbering has a geometrical interpretation, as we shall see now.

\subsection{Approximants and conumbering}

\begin{figure}[htp]
\centering
\includegraphics[width=.8\textwidth]{img1/cut_and_project_n_5.pdf}
\caption{The cut and project construction of the approximant $\irr = 5/8$ of the Fibonacci chain. The conumbering of each site is shown below it.}
\label{fig:conumbering}
\end{figure}
Fig~\eqref{fig:conumbering} shows an approximant of the Fibonacci chain, together with the conumbering of each of its sites. As one can see, the conumbering of a site exactly corresponds to the ordering of the site in perpendicular space $E_\perp$. Let us prove it.

We consider an approximant of slope $\irr_l = p_l/q_l$. 
In this case, the structure is periodic, with a unit cell of $L_l = p_l+q_l$ sites.
We have successively
\begin{align}
	\nperp{\irr_l}{m} & = \fpart*{\frac{q_l m}{L_l}} \\
	\nperp{\irr_l}{m} & = \frac{1}{L_l} \left(q_l m \mod L_l \right)
\end{align}
So, the normalized perpendicular coordinate is proportional to the integer
\begin{equation}
	c = q_l m \mod L_l
\end{equation}
which is nothing but the conumber of site $m$ (see Eq.\ \eqref{eq:conumbering}), as announced.
For example, in the case of the Fibonacci chain, defining the l$^\text{th}$ approximant as the l$^\text{th}$ convergent of $\tau^{-1}$: $\irr_l = F_{l}/F_{l+1}$, we obtain a periodic chain of period $F_{l+2}$, and the conumbering is given by
\begin{equation}
	c = F_{l+1} m \mod F_{l+2}.
\end{equation}
This formula can moreover easily be inverted, yielding
\begin{equation}
	m = (p_l' + q_l')c \mod L_l.
\end{equation}
In the case of the Fibonacci chain, we have
\begin{equation}
	m = (-1)^l F_{l+1} c \mod F_{l+2}
\end{equation}