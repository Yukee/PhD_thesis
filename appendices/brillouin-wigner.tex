\chapter{Brillouin-Wigner perturbation theory}

Suppose we have a Hamiltonian
\begin{equation}
	\op{H} = \op{H}_0 + \lambda \op{H}_1
\end{equation}
with $\op{H}_0$ and $\op{H}_1$ of the same order of magnitude, and $\lambda \ll 1$. 
The goal of perturbation theory is to express order by order in $\lambda$ the energies and eigenstates of $\op{H}$ in terms of the energies and eigenstates of $\op{H}_0$.
Brillouin-Wigner perturbation theory reaches this goal by writing an effective Hamiltonian which is easier to treat that the full Hamiltonian.

Let $\op{Q}$ denote the projector on the eigenspace of $\en$, energy of the unperturbed Hamiltonian $\op{H}_0$.
Let us call $\op{P}$ the projector orthogonal to $\op{Q}$.
We are only interested in what happens in the subspace $\op{Q}$. Our goal is to write an effective Hamiltonian $\op{H}_\text{eff}$ which coincides with $\op{H}$ on $\op{Q}$, and which is easy to approximate.

To this end, let us chose $\ket{\psi}$, an eigenstate of $\op{H}$ associated to the energy $E$.
We wish to express $\ket{\psi}$ in terms of its projection $\op{Q}\ket{\psi}$ only.
We have successively
\begin{align}
	(E-\op{H}_0) \Ket{\psi} 		& = \lambda \op{H}_1 \Ket{\psi} \\
	(E-\op{H}_0) \op{P} \Ket{\psi}	& = \lambda \op{P} \op{H}_1 \Ket{\psi} \\
	\op{P} \Ket{\psi} 					& = \lambda \op{P} \frac{1}{E - \op{H}_0} \op{H}_1 \Ket{\psi}
\end{align}
and thus
\begin{align}
	\Ket{\psi} &= (\op{P} + \op{Q})\Ket{\psi} \\
	\Ket{\psi} &= \op{Q}\Ket{\psi} + \lambda \op{P} \frac{1}{E - \op{H}_0} \op{H}_1 \Ket{\psi}
\end{align}
Iterating the previous relation, and we obtain
\begin{equation}
	\ket{\psi} = \left( 1 + \lambda \op{P} \frac{1}{E-\op{H}_0} \op{H}_1 + \lambda^2 \op{P} \frac{1}{E-\op{H}_0} \op{H}_1 \op{P} \frac{1}{E-\op{H}_0} \op{H}_1 + \dots \right) \op{Q} \Ket{\psi}
\end{equation}
which we can write more compactly as
\begin{equation}
	\ket{\psi} = \sum_{n = 0}^{\infty} \left(\lambda \op{P} \frac{1}{E-\op{H}_0} \op{H}_1\right)^n \op{Q} \Ket{\psi}
\end{equation}
Applying $\op{Q}\op{H}$ on $\ket{\psi}$, we then obtain 
\begin{equation}
	E \op{Q} \ket{\psi} = \left( \en + \op{Q} \op{H}_1 \sum_{n = 0}^{\infty} \lambda^{n+1} \left(\op{P} \frac{1}{E-\op{H}_0} \op{H}_1\right)^n \right) \op{Q} \ket{\psi}
\end{equation}
Therefore, on the subspace $\op{Q}$ the full Hamiltonian $\op{H}$ can be replaced by the effective Hamiltonian
\begin{equation}
	\op{H}_\text{eff} = \en + \op{Q} \op{H}_1 \sum_{n = 0}^{\infty} \lambda^{n+1} \left(\op{P} \frac{1}{E-\op{H}_0} \op{H}_1\right)^n
\end{equation}
which we can write in a more symmetric fashion:
\begin{equation}
\boxed{
\begin{aligned}
	\op{H}_\text{eff} = & \op{Q} \op{H}_0 \op{Q} + \lambda \op{Q} \op{H}_1 \op{Q} + \lambda^2 \op{Q} \op{H}_1 \op{P} \frac{1}{E-\op{H}_0} \op{P} \op{H}_1 \op{Q} + \\
	 & \lambda^3 \op{Q} \op{H}_1 \op{P} \frac{1}{E-\op{H}_0} \op{P} \op{H}_1 \op{P} \frac{1}{E-\op{H}_0} \op{P} \op{H}_1 \op{Q} + \dots
\end{aligned}
}
\end{equation}
Remark that we have made no approximation: on subspace $\op{Q}$, $\op{H}_\text{eff}$ coincides with the full Hamiltonian. The approximation then consists in cutting the formula at a given order in $\lambda$.
 
Remark that Brillouin-Wigner perturbation theory and standard (Rayleigh-Schrödinger) perturbation theory are equivalent at first order in $\lambda$.
At higher orders, Brillouin-Wigner perturbation theory has the advantage of treating on equal footing degenerate and non-degenerate levels. Moreover, the order $\lambda^n$ term writes in a compact formula, contrary to Rayleigh-Schrödinger perturbation theory.
However, Brillouin-Wigner perturbation theory furnishes only an implicit equation on the perturbed energy $E$.
This is not a problem if we are only interested in writing $\op{H}_\text{eff}$ at the lowest non-vanishing order in $\lambda$: we can then replace $E$ by $\en$, the unperturbed energy.

