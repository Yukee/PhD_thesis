\chapter{Renormalization group for the eigenstates of the Fibonacci chain}
\label{app:renorm}

In this appendix, we derive the implicit equation the average fractal dimensions of the eigenstates obey.
In the course of doing so, we find the analytical expression for the renormalization factors $\lambda$ and $\lb$ (equation \eqref{eq:lb}).

the $q$-weight for an energy level $E$ writes
\begin{equation}
	\chi^{(l)}(E) = \sum_{m=1}^{F_l} |\psi_m(E)|^{2q}
\end{equation}
We are also going to define the partial sums on atomic (A) or on molecular (M) sites only:
\begin{equation}
	\chi_{A/M}^{(l)}(E) = \sum_{m, \text{~at/mol}} |\psi_m(E)|^{2q}
\end{equation}

\emph{Atomic energy levels.}
Let us assume that $E$ is an energy in the atomic cluster at step $n$.
Using the tight-binding equations, we can relate at leading order in $\rho$ the amplitudes on molecular sites to the amplitude on neighboring atomic sites.
Using our Ansatz for the eigenstates, we relate the amplitude on atomic sites at step $n$ to the amplitude on atomic and molecular sites at step $n-3$.
\begin{equation}
	\chi^{(l)}(E) = \lb^q \left( (1+2\rho^{2q}+\rho^{4q})\chi^{l-3}(E') + \rho^{4q}\chi_A^{l-3}(E') \right)
\end{equation}

\emph{Molecular energy levels.}
Using the tight-binding equations, we can relate the amplitudes on atomic sites to the amplitude on neighboring molecular sites. Specifically, we write the $q$-weight on an atomic site $m$ surrounded by two molecular sites $m-1$ and $m+1$ as
\begin{equation}
|\psi_m|^{2q}  = \gamma_q(\rho) \rho^{2q} (|\psi_{m-1}|^{2q}+|\psi_{m+1}|^{2q})
\end{equation}
where we have introduced the function $\gamma_q(\rho)$ that takes into account the possible interferences bewteen the site $m-1$ and $m+1$. We have the constraints $\gamma_0(\rho) = 1/2$, $\gamma_{q \neq 0}(0) = 1$, $\gamma_q(1) = 1/2$. 
We chose for this parameter the form $\gamma_q(\rho) = 1/(1+\rho^{2q})$, which is supported by numerical evidences.
Then, the total $q$-weight on a molecular level writes
\begin{equation}
	\chi^{(l)}(E) = \lambda^q \left( (2+\gamma_q \rho^{2q})\chi^{l-2}(E') + \gamma_q \rho^{2q} \chi_A(E') \right)
\end{equation}

\emph{The special case $q=1$.}
When $q=1$, the $q$-norm is independant of $l$. Therefore, we have a closed system of equations for $\lambda$ and $\lb$.
It is straightforward to solve it, and we obtain the expressions given by equation \eqref{eq:lb}.

\emph{The general case: $q$ arbitrary.}
Now the $q$-norm depends on $n$. To obtain a closed system of equations, we take the limit $l \rightarrow \infty$, knowing that the limit behavior of the $q$-norm is $\log \chi^{(l)}_q \sim (q-1)\avwf_q \log (1/F_l)$.
After averaging over the energies, we obtain as an implicit equation
\begin{equation}
\label{eq:implicit}
	2 \omega^2 (2\omega^2 I_{MM} + \omega^3 I_{AM}) + \omega^3 ( 2 \omega^2 I_{MA} + \omega^3 I_{AA}) = 1
\end{equation}
with
\begin{align*}
	I_{MM} & = \left( 2+2\rho^{2q}\gamma_q(1 - M(\tau,q)) \right) M(\tau,q)\\
	I_{AM} & =  \left( 2+\rho^{2q}\gamma_q(1 + A(\tau,q)) \right) M(\tau,q)\\
	I_{MA} & =\left( 1 + 2\rho^{2q} + 2\rho^{4q}(1-M(\tau,q)) \right)A(\tau,q)\\
	I_{AA} & =  \left( 1 + 2\rho^{2q} + \rho^{4q}(1+A(\tau,q)) \right)A(\tau,q)
\end{align*}
and the ``molecular'' and ``atomic'' coefficients given by
\begin{align*}
	M(\tau,q) &  = \omega^{-2\tau} \lambda^q \\ 
	A(\tau,q) & = \omega^{-3\tau} \lb^q
\end{align*}
Then, for a given $q$, $\tau = \tau_q^\psi$ is the solution of the implicit equation \eqref{eq:implicit}. The averaged fractal dimensions of the eigenstates are given by $\avwf_q = \tau_q^\psi/(q-1)$.

\textbf{Perturbative expression in the strong modulation limit:}\\
Neglecting terms of order $\rho^{4q}$ in the above expression, we obtain
\begin{align*}
	I_{MM} \sim I_{AM}& \sim \lambda(\rho)^q/\lambda(\rho^q) \omega^{-2\tau} \\
	I_{MA} \sim I_{AA}&\sim \lb(\rho)^q/\lb(\rho^q) \omega^{-3\tau}.
\end{align*}
Then, from \eqref{eq:implicit} we get the perturbative formula \eqref{eq:wf_av}.

The derivation of the implicit relation for the averaged local spectral dimensions (equation \eqref{eq:avspec}) is in the same lines. The final relation is much simpler because we have dropped the terms of order $\rho^{4q}$. This is in order to be consistent with the results for the spectrum, that are only valid at leading order in $\rho$.
