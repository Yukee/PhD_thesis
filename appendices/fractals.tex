\chapter{Fractals}
\label{app:fractals}

\begin{figure}[htp]
\centering
\includegraphics[width=.4\textwidth]{img1/Fractal_Broccoli.jpg}
\caption{A Romanesco broccoli. Image credits: Jon Sullivan (public domain).}
\label{fig:romanesco}
\end{figure}

In the course of the study of quasiperiodic Hamiltonians, we frequently encounter fractal objects.
A fractal is a self-similar object: it exhibits patterns which repeats at all -- or at least on many -- length scales.
We will also say that a fractal object exhibit \emph{scaling symmetry}.
Romanesco broccoli (Fig.~\eqref{fig:romanesco}) are popular examples of fractal objects.

There are of course scale invariant objects which are not fractal.
A trivial example is the unit segment $[0,1]$.
Zooming on any part of this segment, one gets a picture similar to the original one. 

% Introduction of the Cantor set: construction and scale-invariance
\section{Scale invariance property}
\begin{figure}[htp]
\centering
\includegraphics[width=0.6\textwidth]{img1/cantor_set.pdf}
\caption{Recursive construction of the Cantor set: a initial interval of length 1 (step 0) is subdivided into 3 equal parts, and the middle part is removed (step 1). The process is then applied recursively to the remaining pieces (step 2 and next).}
\label{fig:cantor_iter}
\end{figure}
We consider another famous example of fractal: the Cantor set. 
This example is particularly relevant as the energy spectrum of certain quasiperiodic Hamiltonian has a Cantor set structure (see Chap. \ref{chap:qp_chains}).
The Cantor set can be built by recursively removing pieces from a segment, as shown on Fig.~\eqref{fig:cantor_iter}.
The Cantor set is self-similar: zooming in on any part of it one obtains the same picture. 

% introduction of the generic description of a fractal set
The scaling properties of the Cantor set differ from the ones of the segment.
How can we characterize this difference?
Let us consider a generic fractal (or non fractal) object.
We will characterize our object by a mass distribution $\mass$.
We shall note $\mass(\mathcal{R})$ the mass inside the region of space $\mathcal{R}$.
The ``mass'' needs not be actual mass, but any physical quantity we would like to analyze.
For example one could study stars in a galaxy, and the ``mass'' affected to each star could be its luminosity.
In future examples concerning quasicrystals, the electronic density will often play the role of mass.

\section{Measuring fractal set}
\subsection{The \bc\ algorithm}
% Introduction of the box-counting algorigthm
\begin{figure}[htp]
\centering
\includegraphics[width=0.6\textwidth]{img1/cantor_boxes.pdf}
\caption{Successive partitions of the Cantor set into boxes of common length. $M_0(\mathcal{B})$, the \bc\ mass associated to the partition $\mathcal{B}$, is also given.}
\label{fig:cantor_bc}
\end{figure}
To determine whether the object we consider is fractal or not, we analyze how its mass distribution changes when length scales are changed.
The \emph{box-counting algorithm} is a way to do it.
Consider a partition of the space into a set of boxes of a given length $l$. 
Let us call the set of boxes $\boxset_l$.
Fig.~\eqref{fig:cantor_bc} shows set of boxes at different length scales, in the case of the Cantor set.
Let us now introduce the \emph{\bc\ mass at scale $l$}:
\begin{equation}
\label{eq:bc_mass}
	\qweight_0(\mu;l) = \# \left\{ \text{boxes in $\boxset_l$ contaning some mass} \right\}
\end{equation}
The index $0$ will become useful later, when we will introduce other ways of approximating the mass at scale $l$.

Fig.~\eqref{fig:cantor_bc} shows how the \bc\ is computed for the Cantor set.
Since $M_0(1/3^n) = 2^n$, by a change of variables one can compute the scaling of the \bc\ mass:
\begin{equation}
\label{eq:bc_cantor}
	\qweight_0^\text{Cantor}(l) = l^{-\frac{\log 2}{\log 3}}
\end{equation}
Moreover, in the trivial case of a segment (or a 1D wire), the \bc\ mass is simply equal to the number of boxes. 
Therefore, the \bc\ mass scales as $1/l$.
Similarly, in the case of a $d$-dimensional object of homogeneous mass,
\begin{equation}
\label{eq:bc_hom}
	\qweight_0^\text{hom}(l) \propto l^{-d}
\end{equation}
Comparing equations \eqref{eq:bc_cantor} and \eqref{eq:bc_hom}, it is tempting to say that the dimension of the Cantor set is $\log 2/\log 3$. 
It is also natural: on the one hand, the Cantor set seems to be less than a 1D object since it is constructed by removing most of the initial interval (in fact, the Cantor set has mass 0 if one constructs it from an initial interval of uniform linear mass), and on the other hand, it seems to be more than a 0D point since it has by construction no isolated points.
So its dimension should be in between 0 and 1: $0 < \log 2/\log 3 \simeq 0.63 < 1$.
Based on these observations, we introduce the \emph{\bc\ dimension}, defined by as
\begin{equation}
	d_0(\mass) = \lim_{l \to 0} - \frac{\log \qweight_0(\mass;l)}{\log l}.
\end{equation}
The \bc\ dimension of the Cantor set is then
\begin{equation}
	d_0^\text{Cantor} = \frac{\log 2}{\log 3}.
\end{equation}

\subsection{The Hölder pointwise exponent}
\label{app:sec:holder}
% Introduction of the Hölder pointwise exponent and use for justifying the bc dimension in the case of monofractal set
\begin{figure}[htp]
\centering
\includegraphics[width=0.6\textwidth]{img1/boxes_cantor_set.pdf}
\caption{A series of boxes shrinking to the point $x=2/9$ of the Cantor set. In blue: the part of the Cantor set that is inside a given box.}
\label{fig:CantorBalls}
\end{figure}

While the \bc\ dimension quantifies how the object mass scales globally, the \emph{pointwise Hölder exponent} quantifies how the object mass scales locally.
Call $B(l,x)$ a box of length $l$ containing point $x$. In the 1D case, boxes are simply intervals.
The pointwise Hölder exponent at point $x$ is
\begin{equation}
\label{eq:holder}
	\hold(x) = \lim_{l \to 0} \frac{\log \mass(B(l,x))}{\log l}
\end{equation}
For an object of homogeneous mass density, the Hölder exponent is also uniform an equal to the space dimension:
\begin{equation}
	\hold^\text{hom}(x) = d
\end{equation}
So, we expect the Hölder exponent to be yet another measure of the dimensionality of a fractal object.
Consider again the Cantor set, and let us evaluate the Hölder exponent at point $x = 2/9$. 
We choose, as shown on Fig.~\eqref{fig:CantorBalls}, a series of boxes shrinking to $x$:
\begin{align*}
	B_1  & = \left[0, \frac{1}{3} \right] \\
	B_2 & = \left[\frac{2}{9}, \frac{1}{3} \right] \\
	B_3 & = \left[\frac{2}{9}, \frac{2}{9} + \frac{1}{3^3} \right]  \\
	& \vdots \\
	B_n & = \left[\frac{2}{9}, \frac{2}{9} + \frac{1}{3^n} \right]
\end{align*}
This series of boxes is represented on figure \eqref{fig:CantorBalls}. 
We compute the length of each box and the mass inside:
\begin{align*}
	|B_1|  & = \frac{1}{3} & \mass(B_1) & = \frac{1}{2} \\
	|B_2| & = \frac{1}{9} & \mass(B_2) & = \frac{1}{4} \\
	|B_3| & = \frac{1}{3^2} & \mass(B_3) & = \frac{1}{2^3}\\
	 &\vdots  & \vdots \\
	|B_n| & = \frac{1}{3^n} & \mass(B_n) & = \frac{1}{2^n}
\end{align*}
Using the definition of the Hölder exponent, we can now compute it:
\begin{align}
	\hold^\text{Cantor}\left( 2/9 \right) & = \lim_{n \to \infty} \frac{\log \mass(B_n)}{\log |B_n|} \\
	\hold^\text{Cantor}\left( 2/9 \right) & = \frac{\log 2}{\log 3}
\end{align}
So the Hölder exponent at point $2/9$ is equal to the \bc\ dimension of the Cantor set.
The Cantor set is uniformly self-similar: to get a structure similar to the whole Cantor set, we can rescale any part of it with the same scaling factor.
One says that the Cantor set is a \emph{monofractal}.
Owing to the monofractal nature of the Cantor set, it is clear that the Hölder exponent is uniform:
\begin{equation}
	\hold^{Cantor}(x) = \frac{\log 2}{\log 3}
\end{equation}
So the Hölder exponent of the Cantor set is uniform and equal to its \bc\ dimension.
These properties are characteristic of a monofractal object.
But what about fractal objects which are not uniformly self-similar: multifractals?

%%%%%%%%%%%% DISCUSSION OF THE SIERPINSKI TRIANGLE IN TERMS OF HOLDER EXPONENT %%%
%\begin{figure}[htp]
%\centering
%\includegraphics[width=0.9\textwidth]{img1/sierpinski_iter.pdf}
%\caption{An iterative construction of the Sierpiński triangle: a triangle is subdivided into 3 smaller triangles, and the construction is repeated for each of the 3.}
%\label{fig:sierpinski_iter}
%\end{figure}
%%
%\begin{figure}[htp]
%\centering
%\includegraphics[width=0.9\textwidth]{img1/sierpinski.pdf}
%\caption{The Sierpiński triangle has a discrete scaling symmetry, as shown here.}
%\label{fig:sierpinski_inv}
%\end{figure}
%The \emph{Sierpiński triangle} is an example of a fractal. 
%It can be constructed by interatively gluing segments, as shown on Fig.~\eqref{fig:sierpinski_iter}.
%While the segment has \emph{continuous} scale invariance, the scale invariance of the Sierpiński triangle is \emph{discrete}: as seen on Fig.~\eqref{fig:sierpinski_inv}, only special, well-chosen scaling factors leave the objet invariant. 
%
%Let us imagine that our Sierpiński triangle is made out of wire of uniform linear mass density.
%How is the mass distributed in the triangle?
%Each of the three smaller triangle (like the one circled on Fig.~\eqref{fig:sierpinski_inv}) is 2 times smaller than the original triangle, and contain 3 times less mass.
%So, changing the length scales by a factor of 2 changes the mass by a factor of 3.
%This is a bit surprising:  for a usual 1D object (like our segment), changing the length scales by a factor of 2 changes the mass by a factor of 2, and for a usual 2D object, changing the length scales by a factor of 2 changes the mass by a factor of 4.
%So, looking at the Sierpiński triangle in the light of how its mass changes under rescaling, \emph{it appears to be in between a 1D and 2D object.}
%
%For an object of uniform mass distribution, the mass $\mass(x,L)$ in a region of size $L$ centered about the point $x$ of the object scales as 
%\begin{equation}
%	\mass(x, L) \propto L^d
%\end{equation}
%if the mass distribution is $d$ dimensional.
%Inspired by this result, we define the \emph{pointwise Hölder exponent} as 
%\begin{equation}
%	\hold(x) = \lim_{L \to 0} \frac{\log \mass(x, L)}{\log L}
%\end{equation}
%In the case of an object of uniform mass, $\hold(x) = d$, the space dimension. 
%In the case of the Sierpiński triangle, since rescaling by a factor of 2 changes mass by a factor of 3.
%In Fig.~\eqref{fig:sierpinski_inv}, we have chosen a zoom in to point $x$ located somewhere in the left triangle, but since every part of the triangle is similar to the triangle itself, the result would be the same for any point choice of $x$. 
%Thus,
%\begin{equation}
%	\hold_\text{Sierpiński}(x) = \frac{\log 3}{\log 2} \simeq 1.58.
%\end{equation}
%Thus, the Sierpiński triangle is indeed an object of fractional dimension 
%\begin{equation}
%	2 < d \simeq 1.58 < 3,
%\end{equation}
%in between a 1D and a 2D object.
%
%In general, we will say the an object is fractal if its Hölder exponent $\hold(x)$ is not everywhere equal to its ``naive'' (or topological) dimension. 
%For example, the topological dimension of the Sierpiński triangle is 1, since it is made of 1D wires, but its Hölder exponent is everywhere larger than 1.
%We thus conclude that the Sierpiński triangle is fractal.

\subsection{Multifractals and the generalized fractal dimensions}
\label{app:generalized_fractal_dimensions}
% Multifractals: example of the two-scales Cantor set
\textbf{Two scales Cantor set and multifractals}

\begin{figure}[htp]
\centering
\includegraphics[width=0.5\textwidth]{img1/two_scales_cantor_set.pdf}
\caption{Iterative construction of a two scales Cantor set.}
\label{fig:TwoScales}
\end{figure}

``Real life'' fractals are most of the time \emph{multifractals}, meaning that the mass scales differently when zooming on different parts of the structure, or, said differently, that the Hölder exponent is not uniform.
We introduce here a first, simple example of such a multifractal set: the \emph{two scales Cantor set}.
This set was introduced in a slightly more general setting in \cite{FractalsHalsey}.

To construct the two scales Cantor set, let us choose two lengths, $l_1$ and $l_2$. 
Instead of cutting the unit interval into 3 pieces of equal length like was done for constructing the usual Cantor set, we cut the unit interval into 3 pieces of length $l_1$, $1-l_1-l_2$ and $l_2$.
Like for the usual Cantor set, the central piece is removed and the cutting procedure is then repeated on the two remaining pieces.
Fig.~\eqref{fig:TwoScales} illustrate the construction process.

Let us now prove that on the two scales Cantor set the Hölder exponent is non-uniform. 
To fix the ideas, we set $l_1 = 1/4$ and $l_2 = 1/2$, like illustrated on Fig.~\eqref{fig:TwoScales}.
Let us evaluate the Hölder exponents of the two end points of the unit interval, $x=0$ and $x=1$.
We consider series of intervals shrinking to these points:
\begin{align*}
	B_1 ^{(1)} & = \left[0, \frac{1}{4} \right] & B_1^{(2)} & = \left[\frac{1}{2}, 1 \right] \\
	B_2^{(1)} & = \left[0, \frac{1}{4^2} \right] & B_2^{(2)} & = \left[ 1 - \frac{1}{4}, 1 \right] \\
	& \vdots & & \vdots \\
	B_n^{(1)} & = \left[0, \frac{1}{4^n} \right] & B_n^{(2)} & = \left[ 1 - \frac{1}{2^{n}}, 1 \right]
\end{align*}
Like previously, we compute the length of the intervals and the mass inside:  for the first series, shrinking to $x=0$, one has $|B_n^{(1)}| = 1/4^{n}$, $\mass(B_n^{(1)}) = 1/3^{n}$, and for the second series, shrinking to $x=1$, one has $|B_n^{(2)}| = 1/2^{n}$, $\mass(B_n^{(2)}) = (2/3)^{n}$.
Computing the Hölder exponents is now straightforward:
\begin{align}
	\alpha(0) = \frac{\log 3}{\log 4} \simeq 0.79  & & \alpha(1) = \frac{\log 3/2}{\log 2} \simeq 0.58 
\end{align}
The Hölder exponent $\alpha(x)$ is indeed non-uniform in the case of the two scales Cantor set.

% Distribution of Hölder exponents
The whole set of $x$ dependent Hölder exponents can be cumbersome to compute in practice.
Let us forget about the precise form of the function $\hold(x)$. 
Consider the set of points $x$ such that $\hold(x) = \hold$. 
In the simple case of a monofractal like the Cantor set, this set is just the whole fractal object, but in the case of a multifractal object like the two scales Cantor set, it is subset of the whole fractal object.
This subset also constitute a generically fractal object.
Let us call $f(\hold)$ its \bc\ dimension.
Calling $\mass_{\hold}$ the mass distribution of the the set of points such that $\hold(x)= \hold$, $f(\hold)$ is such that
\begin{equation}
\label{eq:f}
	M_0(\mass_{\hold};l) \simlim{l \to 0} l^{-f(\hold)}
\end{equation}
where $M_0$ is the \bc\ mass introduced in Eq.~\eqref{eq:bc_mass}.
Since $M_0$ counts the number of occupied boxes, $l^{-f(\hold) + 1}$ is proportional to the probability that the Hölder exponent equals $\hold$.
Thus, \emph{$f(\hold)$ contains the information about the scaling of the Hölder exponents' distribution}. 
For that reason, we shall call $f(\hold)$ the \emph{multifractal spectrum}.
A multifractal spectrum reduced to a point is the signature of either a trivial (non-fractal) object, or of a monofractal, while a non-trivial multifractal spectrum is the signature of a multifractal object.

% introduction of the generalized fractal dimensions
We now link the multifractal spectrum with the \bc\ dimension.
First, we need to generalize the \bc\ mass introduced in Eq.~\eqref{eq:bc_mass}.
Consider as before $\boxset_l$, a partition of space into boxes of length $l$.
Denote by $\mass(B)$ the mass contained in box $B$, and $\mass_\text{tot}$ the total mass of the object.
Let us introduce the quantity
\begin{equation}
\label{eq:qmass}
	\qweight_q(\mass;l) = \sum_{B \in \boxset_l} \left(\frac{\mass(B)}{\mass_\text{tot}}\right)^q,
\end{equation}
which we shall call the $q$-mass in this thesis.
Remark that $M_0$ corresponds to the \bc\ mass introduced in Eq.\ \eqref{eq:bc_mass}.
% interpretation of the q mass
When $q$ is large, only the largest terms in the sum \eqref{eq:qmass} contribute significantly to the $q$-mass. 
Therefore, a $q$-mass with $q$ large probes the regions where the mass density is the largest.
Similarly, when $q$ large in absolute value and negative, regions where the mass density is the smallest are probed.
So, varying $q$ enables us to capture more information about the scaling of the object.

For an $d$-dimensional homogeneous mass distribution, the $q$-mass scales as
\begin{equation}
	\qweight^\text{hom}_q(\mass;l) \simlim{l \to 0} l^{(q-1)d}
\end{equation}
In the light of this result, we introduce the \emph{generalized fractal dimensions}:
\begin{equation}
\label{eq:fractal_dims}
	d_q(\mass) = \lim_{l \to 0} \frac{1}{q-1}\frac{\log \qweight_q(\mass;l)}{\log l}
\end{equation}
In particular, $d_0$ is indeed the \bc\ dimension introduced before.
$d_1$ is sometime called the information dimension, and $d_2$ the correlation dimension.

% Link with box-counting
We know make the connection between the $q$-mass and the multifractal spectrum.
Calling $B(x,l)$ the box of length $l$ shrinking to point $x$, we rewrite the $q$-mass \eqref{eq:qmass} as
\begin{equation}
	\qweight_q(\mass; l) = \sum_x \left( \frac{\mass(B(x,l)}{\mass_\text{tot}} \right)^q 
\end{equation}
Recalling the definition of the pointwise Hölder exponents \eqref{eq:holder} and of the multifractal spectrum \eqref{eq:f}, we have successively:
\begin{align}
	\qweight_q(\mass;l) & \simlim{l \to 0}  \sum_{x} l^{q \hold(x)} \\
	\qweight_q(\mass;l) & \simlim{l \to 0} \sum_{\hold} M_0(\mass_{\hold};l) l^{q \hold} \\
	\qweight_q(\mass;l) & \simlim{l \to 0} \sum_{\hold} l^{q \hold - f(\hold)}
\end{align}
When $l \to 0$, the sum is dominated by the term which minimizes $q \hold - f(\hold)$.
Let us call this term $\hold_q$.
We have
\begin{equation}
	\qweight_q(\mass;l) \simlim{l \to 0} l^{q \hold_q - f(\hold_q)}
\end{equation}
Comparing with the definition of the generalized fractal dimensions \eqref{eq:fractal_dims}, we end up with a relation between the multifractal spectrum and the generalized dimensions:
\begin{equation}
\label{eq:multifractal_spec}
	(q-1)d_q = q \hold_q - f(\hold_q).
\end{equation}

\begin{figure}[htp]
\centering
\includegraphics[width=0.5\textwidth]{img1/tau_legendre_transform.pdf}
\caption{Graphical construction of the multifractal spectrum at point $q_0$. In blue, the $\tau_q$ function. In red, the tangent to $\tau_q$ at point $q_0$. The vertical intercept of the tangent is $-f(\alpha_{q_0})$.}
\label{fig:legendre}
\end{figure}
Let us define
\begin{equation}
	\tau_q = (q-1)d_q.
\end{equation}
Remark that $\alpha_q = \tau'_q$.
Using this property, and the relation \eqref{eq:multifractal_spec}, the mulifractal spectrum can be computed graphically from $\tau_q$.
More precisely, and as shown on Fig.~\eqref{fig:legendre}, \emph{the multifractal spectrum $f(\hold)$ is the Legendre transform of $\tau_q$}.

\section{Computing fractal dimensions and the multifractal spectrum}
\label{sec:multifractal}
Fractal dimensions and the multifractal spectrum contain the same information since they are Legendre transform of one another.
It is often interesting to compute both to gain more knowledge about the fractal set at hand.

A practical way of computing $f(\hold)$ is to  compute first the generalized fractal dimensions $d_q$ using the so-called \emph{partition function}. 
Then we can access the multifractal spectrum using the fact that $\hold_q = \tau'_q$ (we recall that $\tau_q = (q-1)d_q$), and $f(\alpha_q) = q \alpha_q - \tau_q$.

As before, consider a partition of the space in boxes.
This time, we allow the boxed to have different lengths, and we call $l(x)$ the length of the box $B(x,l)$, shrinking to point $x$. Allowing boxes to have different sizes can be handy for numerical computations.
The partition function $\Gamma$ is defined by
\begin{equation}
\boxed{
	\Gamma(q,\tau) = \sum_x \frac{\mass(B(x,l))^q}{l(x)^\tau}
}
\end{equation}
We have $\Gamma(q,\tau > \tau_q) \to + \infty$, and $\Gamma(q,\tau < \tau_q) \to 0$ in the limit $l (x)\to 0$.
Only at $\tau = \tau_q$ does $\Gamma$ have a finite nonzero limit when $l \rightarrow 0$. This gives us a practical way of computing $\tau_q$.

\begin{figure}[htp]
\centering
\includegraphics[width=0.6\textwidth]{img1/two_scales_cantor_set_boxes.pdf}
\caption{Successive partitions of the two scales Cantor set into boxes.}
\label{fig:twoscalescantor_boxes}
\end{figure}

As an illustrative example, let us compute the fractal dimensions and multifractal spectrum of the two scales Cantor set introduced previously (Fig.~\eqref{fig:TwoScales} shows how this set is constructed).
Study of quasiperiodic Hamiltonians will provide us with more physical examples of multifractal spectra.

We partition the two scales Cantor set into boxes, as shown by Fig.~\eqref{fig:twoscalescantor_boxes}.
We then compute $\Gamma^{(n)}(q, \tau)$, the partition function associated to the n$^\text{th}$ partition of the Cantor set.
For $n \geq 1$, 
\begin{equation}
	\Gamma^{(n+1)} = \left( \Gamma^{(n)} \right)^2
\end{equation}
Moreover,
\begin{equation}
	\Gamma^{(1)}(q, \tau) = \frac{\left( \frac{1}{3} \right)^q}{\left( \frac{1}{4} \right)^\tau} + \frac{\left( \frac{2}{3} \right)^q}{\left( \frac{1}{2} \right)^\tau}
\end{equation}
The partition function has a finite nonzero limit when $n \to \infty$ (\ie\ when the length of the boxes goes to 0) if and only if $\Gamma^{(1)} = 1$.
Therefore, $\Gamma^{(1)}(q, \tau_q) = 1$ and $\tau_q = \log x/ \log 2$ with
\begin{equation}
	x = 2^{q-1} \left( \sqrt{1+ 4 \left( \frac{3}{4} \right)^q} -1 \right)
\end{equation}
In particular, the \bc\ dimension is $d_0 = \log \tau / \log 2$, where $\tau = (1 + \sqrt{5})/2$ is the golden ratio.
Once $\tau_q$ is known, it is not difficult to compute the multifractal spectrum.
Fig.~\eqref{fig:spectrum_cantor_set} shows it.

\begin{figure}[htp]
\centering
\includegraphics[width=0.6\textwidth]{img1/f_alpha_2_scales_cantor_set.pdf}
\caption{The multifractal spectrum of the two scales Cantor set.}
\label{fig:spectrum_cantor_set}
\end{figure}

\textbf{Generic properties of the multifractal spectrum}

\begin{itemize}
	\item As a Legendre transform of a concave function (see Fig.~\eqref{fig:legendre}), the multifractal spectrum must be a concave function.
	So, the multifractal spectrum has at most one maximum, reached at point $\alpha_{0}$.
	The bell shape of the multifractal spectrum observed on Fig.~\eqref{fig:spectrum_cantor_set} is thus generic.
	\item The slope of the multifractal spectrum verifies
	\begin{equation}
		q = f'(\alpha_q)
	\end{equation}
	\item In particular, $f(\alpha_0) = d_0$.
	\item $\alpha_\text{min} = d_{+ \infty}$, $\alpha_\text{max} = d_{- \infty}$.
\end{itemize}

\section{Statistical physics and multifractal analysis}
In this section, we discuss two completely independent connections between statistical physics and multifractal analysis.
\subsection{Multifractal analysis in the language of statistical physics}
There is a formal analogy between the concepts of statistical physics and those of multifractal analysis, as discussed in details in \cite{kohmoto1988entropy}.
Let us consider as before $\mathcal{B}_l$, a partition of our space into a set of boxes. Indexing the boxes in this partition with an integer $i$, and calling $\mu_i$ the mass in the i$^\text{th}$ box, we can rewrite the $q$-mass \eqref{eq:qmass} as
\begin{equation}
	\qweight_q(\mu;l) = \sum_i \left( \frac{\mu_i}{\mu_\text{tot}} \right)^q.
\end{equation}
Recalling the definition of the pointwise Hölder exponent \eqref{eq:holder}, this equation can then be recast as
\begin{equation}
	\qweight_q(\mu;l) = \sum_i e^{-\beta_q(l) \hold_i},
\end{equation}
where we have introduced the \emph{inverse temperature} $\beta_q(l) = q |\log l|$.
Now, the link with statistical physics is evident.
Interpreting the space our mass distribution lives in as the phase space of some statistical physics system, and interpreting $\beta_q(l)$ as the inverse temperature of the system, we conclude that $\exp(-\beta_q(l) \hold_i)$ is proportional to the probability of the system visiting the i$^\text{th}$ box.
The Hölder exponent $\hold_i$ is the energy of the system in this configuration, and the $q$-mass is the partition function of the system.
The free energy of the statistical physics system is proportional to the fractal dimension:
\begin{equation}
	F(\beta_q(l)) \simlim{l \to 0} \frac{q}{q-1} d_q,
\end{equation}
while its average energy coincides with the Hölder exponent of equation \eqref{eq:multifractal_spec}:
\begin{equation}
	\overline{E}(\beta_q(l)) \simlim{l \to 0} \hold_q.
\end{equation}
Finally, the multifractal spectrum is proportional to the entropy:
\begin{equation}
	S(\beta_q(l)) \simlim{l \to 0} |\log l| f(\hold_q).
\end{equation}

\subsection{Rényi entropies and quenches}
The $q$-mass can be given a physical meaning in the context of a quenched statistical physics system \cite{baez2011renyi}, in the following way.
Partition the phase space of a given system into boxes, and call $\mass(B(x))$ the probability that the system's state is in box $B(x)$ centered about $x$. Suppose that the system is at thermal equilibrium at temperature $T$. When the boxes length goes to zero, we have $\mass(B(x)) \sim \exp(-E(x)/T)/Z$ where $E(x)$ is the average energy of the state in box $B(x)$. Remark that the partition function, temperature and energy introduced here have no connection with the quantities defined in the previous subsection. They are the partition function, temperature and energy of a ``real'' statistical physics system, not quantities defined \emph{by analogy} with statistical physics.
We now introduce the Rényi entropy of order $q$:
\begin{equation}
	S_q = \frac{1}{q-1} \log M_q
\end{equation}
Remark that $d_q = \lim_{l \to 0} S_q/\log l$ where $l$ is the boxes width.
Also remark that $S_1$ is equal to the entropy of the system.
Furthermore, if the system is brought by a sudden quench to temperature $T' = T/q$, then the maximum amount of work that can be extracted from the system as it reaches thermal equilibrium at the new temperature $T'$, divided by the change in temperature, equals the system’s Rényi entropy of order $q$ in its original state at temperature $T$.
In other words,
\begin{equation}
	S_{T/T'} = -\frac{F(T') - F(T)}{T' - T}
\end{equation}
where $F$ is the free energy of the system.

\section{Discrete scale invariance and log-periodic oscillations}
\label{app:sec:discrete_scale_invariance}

Fractals being scale invariant distributions of points, a physical quantity defined on a fractal can be modeled as a function with discrete scale invariance. That is a function verifying
\begin{equation}
	bf(x) = f(ax).
\end{equation}
for some fixed $a$.
Let us assume to begin with that the function has \emph{continuous} scale invariance, meaning that the previous equation holds for any $a$.
It is then  an easy exercise to show that the only solutions to this functional equation are the power laws of the form $f(x) = C x^{\log b/\log a}$.

Imposing only discrete scale invariance, we exepct the set of solutions to be larger. In fact, one can prove \cite{lapidus2013fractal, sedgewick2013introduction, akkermans2013statistical} that the solutions are
\begin{equation}
	f(x) = C\left( \frac{\log x}{\log a} \right) x^{\frac{\log b}{\log a}},
\end{equation}
where $C$ is a periodic function of period 1.
Thus, $f$ is a power-law multiplied by a \emph{log-periodic function}.
Such functions are frequently observed in physical systems with some kind of discrete scale invariance, see \eg\ \cite{akkermans2013statistical, khamzin2015analytical}.
The case of quasiperiodic tiling is a bit more complicated. They are not discrete scale invariant objects (the distribution of tiling points is not fractal at all, but perfectly regular). However, they can often be constructed by the discrete \emph{scale invariant process} of substitution (or inflation).
Therefore, it comes as no surprise that many physical quantities on quasiperiodic tilings exhibit power-laws with log-periodic modulations, see \eg\ \cite{lifshitz2011observation, thiem2015origin}.
We shall see three examples of such quantities in this thesis: \ref{sec:transient_and_stable_gaps}, \ref{sec:transmission} and \ref{subsec:b3}.