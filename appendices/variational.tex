\chapter{Variational method}
\label{app:var}

In this appendix we derive the variational equations used in Sec.~\ref{sec:var} for approximation of the 2D groundstate on the \AB\ tiling.

We recall that the Hamiltonian writes 
\begin{equation}
	\op{H}(t,V) = -t \op{H}_0 + V \op{H}_1,
\end{equation}
with 
\begin{equation}
	\op{H}_0 = \sum_{\langle m, n \rangle} \ket{m} \bra{n} + \hc,
\end{equation}
and
\begin{equation}
	\op{H}_1 = \sum_m z_m \ket{m} \bra{m}.
\end{equation}

We work with the following variational wavefunction:
\begin{equation}
	\psi(m) = C_{\mu(m)} e^{\kappa h(m)}
\end{equation}
where $\mu(m)$ is the nearest neighbors configuration of the site $m$: $\mu = $ A, B, C, D$_1$, D$_2$, E, F (see Sec.~\ref{sec:prefactor} for details).

\subsection{The variational energy}

We recall that variational method consists in minimizing the energy $E(\{C\},\kappa) = \Braket{\psi | \op{H} | \psi}/\Braket{\psi | \psi}$ with respect to variational parameters of $\ket{\psi}$, namely here the constant $\kappa$ and the 7 preexponential factors $C_\mu$, $\mu = $ A, B, C, D$_1$, D$_2$, E, F.
We thus have 8 variational equations, the solution of which gives an approximation to the exact groundstate wavefunction of our Hamiltonian.

\vspace{10pt}
\textbf{Evaluation of the norm $\Braket{\psi | \psi}$.}

In the following, we work on a finite-size sample $\reg_t$, and we then let the size of the sample go to infinity ($\reg_t \to \reg_\infty$).
In the same lines as in Sec.~\ref{sec:1Dheight} and in appendix~\ref{sec:2Dheights}, we first compute $N_\mu^{(t)}(h)$, the number of times we have height $h$ on sites of type $\mu$ inside region $\reg_t$.
For that, we introduce the generalized inflation matrix
\begin{equation}
	M(\beta) = \begin{bmatrix}
		1 & 1 & 1 & 1 & 0 & 0 & 0\\
		0 & 0 & 0 & 0 & 1 & 0 & 0\\
		0 & 0 & 0 & 0 & 0 & 1 & 0\\
		0 & 0 & 0 & 0 & 0 & 0 & 1\\
        0 & 0 & 0 & 0 & 0 & 0 & e^{\beta}\\
        0 & 0 & 0 & 0 & 2e^{\beta} & 3e^{\beta} & 2e^{\beta}\\
        8e^{\beta} & 8e^{\beta} & 8e^{\beta} & 8e^{\beta} & 5e^{\beta} & 2e^{\beta} & 0
	\end{bmatrix}.
\end{equation}
This matrix has the same physical interpretation as the generalized inflation matrices introduced in Sec.~\ref{sec:1Dheight} and in appendix.~\ref{sec:2Dheights}.
In particular, $M(0)$ is the geometrical inflation matrix relating the number of sites of type $\mu =$ A, B, \dots after $t$ inflations to the number of sites after $t+1$ inflations.
Let $f(\beta)$ is the eigenvector associated to the largest eigenvalue of $M(-\beta)M(\beta)$.
We have
\begin{align}
\Braket{\psi |\psi} &= \sum_{\mu} C_\mu^2 \sum_h e^{2\kappa h} N_\mu^{(t)}(h) \\
                           &= \sum_{\mu}  C_\mu^2  \gen_\mu^{(t)}( 2\kappa) \\
                           & = \omega^t(2 \kappa) \sum_\mu C_\mu^2 f_\mu(2\kappa)
\end{align}

\vspace{10pt}
\textbf{Evaluation of the average $\Braket{\psi|\op{H}_0|\psi}$.}

\begin{equation}
	\Braket{\psi | \op{H}_0 | \psi} = \sum_{\mu,\nu} C_\mu C_\nu \sum_h e^{\kappa( 2 h+\epsilon(\mu \to \nu))} N_\nu(\mu,h)
\end{equation}

$N_\nu(\mu,h)$ is the number of bonds $(\mu,\nu)$ with $\mu$ having height $h$.
$\epsilon(\mu \to \nu) = \pm 1$ respectively if the arrow goes from $\mu$ to $\nu$ or the reverse.
We can write $N_\nu(\mu,h)=z(\nu|\mu,h)N_\mu(h)$ with $z(\nu|\mu,h)$ the average number of type $\nu$ sites around type $\mu$ sites that have height $h$.
If the number of $\nu$ sites around $\mu$ is always the same, then $z(\nu|\mu,h)=z(\nu|\mu)$.
This is the case for $\mu < \nu$ (here we use lexicographic order: $1=A, 2=B, 3=C, 4=D_1, 5=D_2, 6=E, 7=F$).
Assuming $\mu < \nu$, we have
\begin{align}
	\sum_h e^{2\kappa h} N_\nu(\mu,h) & = z(\nu|\mu)\sum_h e^{2\kappa h} N_\mu(h) \\
	& = z(\nu|\mu)\gen_\mu(2\kappa)
\end{align}
Because the Hamiltonian is real symmetric, $e^{\kappa \epsilon(\mu \to \nu)} N_\nu(\mu,m)$ is symmetric under the exchange of $\mu$ and $\nu$. 
% In particular, if $\mu > \nu$,
% \begin{align}
% 	e^{\kappa \epsilon(\mu \to \nu)} \sum_h e^{2\kappa h} N_\nu(\mu,m) & = e^{\kappa \epsilon(\nu \to \mu)} \sum_m e^{2\kappa h} N_\mu(\nu,h) \\
% 	& = e^{\kappa \epsilon(\nu \to \mu)} z(\mu|\nu) \gen_\nu(2 \kappa).
% \end{align}
So, finally
\begin{equation}
	\Braket{\psi| \op{H}_0|\psi} = \om^t(2 \kappa) \sum_{\mu,\nu} C_\mu h_{\mu,\nu}(2\kappa) C_\nu
\end{equation}
where $h$ is the symmetric $7 \times 7$ matrix 
\begin{align}
	h_{\mu,\nu}(2 \kappa) & = e^{\kappa \epsilon(\mu \to \nu)} z(\nu|\mu) f_\mu(2 \kappa)~\text{if $\mu < \nu$} \\
               & = e^{\kappa \epsilon(\nu \to \mu)} z(\mu|\nu) f_\nu(2 \kappa)~\text{if $\mu > \nu$.}
\end{align}

\vspace{10pt}
\textbf{Evaluation of $\Braket{\psi|\op{H}|\psi}$}

This straightforwardly amounts to replacing $h_{\mu, \nu}$ by
\begin{equation}
	-t h_{\mu,\nu}(2\kappa) + V z_\mu f_\mu(2\kappa) \delta_{\mu,\nu}
\end{equation}
with $z_\mu$ the coordination of type $\mu$ sites.

\subsection{The variational equations}
If the energy has an extrema with respect to $p$, it obeys the equation
\begin{equation}
	 \partial_p \Braket{\psi|\op{H}|\psi} = E(p) \partial_p \Braket{\psi|\psi}.
\end{equation}
Here we have two kinds of parameters: the preexponential factors $C$ and $\kappa$. Let us consider each in turn.

\vspace{10pt}
\textbf{Extremization with respect to $C$}

Let us first extremize for $\op{H}_0$. Since $h$ is symmetric, we have
\begin{equation}
\label{eq:min_c}
	\sum_\nu h_{\mu,\nu} C_\nu = E f_\mu C_\mu
\end{equation}
So, $\mathbf{C}(\kappa)$ is an eigenvector of the matrix $M_{\mu, \nu} = h_{\mu,\nu}(\kappa)/f_\mu(\kappa^2)$, with eigenvalue $E$.
There are thus 7 independent solutions for $\mathbf{C}(\kappa)$, for each value of $\kappa$. 
The extension to $H$ is simple: $M_{\mu, \nu}$ becomes
\begin{equation}
	M_{\mu,\nu} = -t h_{\mu,\nu}(2\kappa)/f_\mu(2\kappa) + V z_\mu \delta_{\mu,\nu}
\end{equation}
Finding the extrema with respect to the $C$ parameters amounts to diagonalizing the matrix $M$. 
Although it is possible to diagonalize $M$ exactly, we do not reproduce the solution here as it is too long.

\vspace{10pt}
\textbf{Extremization with respect to $\kappa$}

Since $h$ is symmetric, we can write 
\begin{equation}
	\Braket{\psi|\op{H}_0|\psi} = 2 \sum_{\mu < \nu} C_\nu z(\nu|\mu)f_\mu(2\kappa)e^{\kappa \epsilon(\mu \to \nu)} C_\mu
\end{equation}
Then, extremization yields
\begin{equation}
\label{eq:min_beta}
	\sum_{\mu < \nu} C_\nu z(\nu|\mu)\partial_\kappa \left (f_\mu(2\kappa)e^{\kappa \epsilon(\mu \to \nu)} \right ) C_\mu = E \sum_\mu C_\mu^2 f'_\mu(2\kappa)
\end{equation}
The extension to $\op{H}$ is, again, straightforward.
We were not able to solve this last equation analytically. 
We instead solved it numerically.
