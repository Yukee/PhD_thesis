# Infos pratiques

[Voir les instructions détaillées](https://www.universite-paris-saclay.fr/fr/la-these-contenu-langue-de-redaction-droit-dauteur-confidentialite-format-et-page-de-couverture)
[Voir les documents utiles](https://www.universite-paris-saclay.fr/fr/doctorat/documents-de-reference/soutenance-de-la-these)

## Page de couverture
* Format de page de couverture imposé pour les versions électroniques d’archivage et de diffusion de la thèse.
* Libre pour les autres, mais les informations règlementaires listées au-dessus doivent impérativement figurer, en particulier le numéro du dépôt légal (NNT ou ISBN).

## Langue de rédaction
* Si anglais, doit faire l'objet d'une demande (auprès de qui ?)
* Si anglais, présence d'un résumé en français (combien de signes ?)

