# -*- coding: utf-8 -*-
"""
Created on Tue Feb  9 12:13:22 2016

@author: nicolas

"""

import networkx as nx
import scipy.linalg as lin
from math import isclose, sqrt
import matplotlib.pyplot as plt
import numpy as np
from scipy.sparse.linalg import eigsh
from scipy.sparse import csr_matrix

def diagonalize(graph):
    """ Manipulatin graphs symbolizing quantum models
    Should derive from the NetworkX class? """

    # return the adjacency matrix of the graph
    h = nx.to_numpy_matrix(graph)
    # diagonalize it
    val, vec = lin.eigh(h)
    
    # ptToVecs[a][pt] is the wf amplitude at energy val[a] and at site pt
    ptToVecs = [dict(zip(graph.nodes(), wf)) for wf in vec.T]
    return val, ptToVecs
    
def replace_nodes(graph, old_nodes, new_nodes):
    """
    Modify graph to replace node old_nodes[i] by new_nodes[i].
    Existing node data is copied.
    """
    # associate the old nodes data to the new nodes
    new_nodes_attr = [(new,graph.node[old]) for old, new in zip(old_nodes, new_nodes)]
    # delete the old nodes
    graph.remove_nodes_from(old_nodes)
    # add the new nodes and their attributes to the graph
    graph.add_nodes_from(new_nodes_attr)
    
def identify_nodes(graph, u, v):
    """  
    Modify graph to identify node v with node u.
    Edges data is also copied.
    """

    weighted_neigh = graph[v] # get the neighbors of v (with the edge data if any)
    ed = [(u, nv, weighted_neigh[nv]) for nv in weighted_neigh]
    graph.add_edges_from(ed)
    graph.remove_node(v)
    
    return None
    
def edge_nodes(g, theta, position_attribute):
    """
    Return the extreme points of g lying in the direction normal to the vector forming an angle theta with the horizontal
    position_attribute is usually 'para' or 'perp'
    """
    # get the positions of the nodes
    pos = np.asarray(list(nx.get_node_attributes(g, position_attribute).values()))
    # angle by which to rotate to bring wanted axis to the horizontal direction
    theta = -theta
    # rotate
    rot_mat = np.array([[np.cos(theta), -np.sin(theta)], 
                         [np.sin(theta),  np.cos(theta)]])
    pos = np.dot(rot_mat, pos.T).T
    # find the extreme points
    x, y = zip(*pos)
    xmax = max(x) # rightmost abscissa
    xmin = min(x)
    ymin = min(y)
    ymax = max(y)
    
    # tolerance is one percent of the average distance between two nodes
    av_dist = min(xmax-xmin, ymax-ymin)/np.sqrt(len(x))    
    tol = 0.01*av_dist

    right_nodes = []
    left_nodes = []

    for p in g.nodes(data=True):
        px, py = np.dot(rot_mat, p[1][position_attribute])
        if np.isclose(px, xmax, atol=tol):
             right_nodes.append(p[0])
        elif np.isclose(px, xmin, atol=tol):
             left_nodes.append(p[0])
             
    return right_nodes, left_nodes

def identify_node_bunch(graph, U, V, new_data = None):
    """
    Modify graph to identify node V[i] with node U[i], without double-counting the inner edges of bunch V.
    Existing edge data is copied.
    new_data is a dict of the type weight='weight', storing new data to add the edges.
    The edge_data argument contains new edge_data that will be added to every new edge.
    Note that self-loops are ignored.
    """

    for u, v in zip(U, V):
        # copy the edge from v to the neighbors of v
        neigh = graph[v]
        # "not in graph.neighbors(u)" ensures that no double-counting is done. However, this also disregard any possible self-loop.
        if new_data: # if there is new data, add it to the existing data
            ed = [(u, nv, {**neigh[nv], **new_data}) for nv in neigh if nv not in graph.neighbors(u)]
        else: # otherwise, only copy the existing data
            ed = [(u, nv, neigh[nv]) for nv in neigh if nv not in graph.neighbors(u)]

        # if the graph is directed we also copy the edge from the neighbors of v to v
        if graph.is_directed():
            pred = graph.predecessors(v)
            if new_data:
                edp = [(pv, u, {**graph[pv][v], **new_data}) for pv in pred if pv not in graph.neighbors(u)]
            else:
                edp = [(pv, u, graph[pv][v]) for pv in pred if pv not in graph.neighbors(u)]
            graph.add_edges_from(edp)

        graph.add_edges_from(ed)
        graph.remove_node(v)
        
    return None
        
def periodize_along(g, direction):
    """
    Modify graph g to be periodic in the direction normal to the vector forming an angle theta with the horizontal.
    Direction is either vertical 'v' or horizontal 'h'.
    """
    if direction == 'h':
        theta = 0.
        idx = 1
    elif direction == 'v':
        theta = np.pi/2.
        idx = 0
    else:
        raise RuntimeError("direction is either vertical 'v' or horizontal 'h'")
             
    right_nodes, left_nodes = edge_nodes(g, theta, 'para')             
    # sort left and right nodes according to their y coordinate, so that left_n[i] and right_n[i] are facing each other
    left_nodes.sort(key=lambda p: g.node[p]['para'][idx])
    right_nodes.sort(key=lambda p: g.node[p]['para'][idx])
        
    # identify right nodes with left nodes
    identify_node_bunch(g, left_nodes, right_nodes, {direction:1.})

def periodize_along_theta(g, theta):
    """
    Modify graph g to be periodic in the direction normal to the vector forming an angle theta with the horizontal.
    """
    
    eps = 0.1
    if np.pi/2.-eps <= theta <= np.pi/2.+eps:
        idx = 0
    else:
        idx = 1
             
    right_nodes, left_nodes = edge_nodes(g, theta, 'para')
    # sort left and right nodes according to their x or y coordinate, so that left_n[i] and right_n[i] are facing each other
    left_nodes.sort(key=lambda p: g.node[p]['para'][idx])
    right_nodes.sort(key=lambda p: g.node[p]['para'][idx])
        
    # identify right nodes with left nodes
    identify_node_bunch(g, left_nodes, right_nodes)
    
def edge_nodes_Penrose(g, theta):
    """
    Return the extreme points of g lying in the direction normal to the vector forming an angle theta with the horizontal
    This version is adapted to the case of the Penrose tiling, where the inflation is performed directly in the physical space.
    """
    # get the positions of the nodes
    pos = np.asarray(g.nodes())
    # angle by which to rotate to bring wanted axis to the horizontal direction
    theta = -theta
    # rotate
    rot_mat = np.exp(1j*theta)
    pos = rot_mat*pos
    # find the extreme points
    x, y = pos.real, pos.imag
    xmax = max(x) # rightmost abscissa
    xmin = min(x)
    ymin = min(y)
    ymax = max(y)
    
    # tolerance is one percent of the average distance between two nodes
    av_dist = min(xmax-xmin, ymax-ymin)/np.sqrt(len(x))
    tol = 0.01*av_dist

    right_nodes = []
    left_nodes = []

    for p in g.nodes():
        pp = rot_mat*p
        px = pp.real
        if np.isclose(px, xmax, atol=tol):
             right_nodes.append(p)
        elif np.isclose(px, xmin, atol=tol):
             left_nodes.append(p)
             
    return right_nodes, left_nodes
    
def periodize_along_theta_Penrose(g, theta):
    """
    Put the graph of a Penrose rhombus on a sphere.
    theta is the direction (wrt to the horizontal) of the vector perpendicular to the vertical faces of the rhombus.
    """
    
    top_nodes, bot_nodes = edge_nodes_Penrose(g, np.pi/2.)
    right_nodes, left_nodes = edge_nodes_Penrose(g, theta)
    
    # sort by x value
    sort = lambda p: p.real
    left_nodes.sort(key=sort)
    right_nodes.sort(key=sort)
    
    top_nodes.sort(key=sort)
    bot_nodes.sort(key=sort)
        
    # identify right nodes with left nodes
    identify_node_bunch(g, left_nodes, top_nodes)
    # remove the leftmost bottom node and the topmost right node from the list, as they are already identified 
    bot_nodes.pop(0)
    right_nodes.pop(0)
    identify_node_bunch(g, bot_nodes, right_nodes)
    return bot_nodes, right_nodes
    
def boundary_hamiltonian(graph, k):
    pv = list(nx.get_edge_attributes(graph, 'v').keys())
    ph = list(nx.get_edge_attributes(graph, 'h').keys())
    if not (pv or ph):
        raise KeyError("Graph has no marked periodized nodes. Try running periodize_along first")
    

def periodize(graph, identify_nodes = True):
    """
    Return a periodized graph.
    This function assumes that nodes of graph are complex numbers, encoding the position of the nodes.
    To each node on at the rightmost abscissa is associated the one on the leftmost abscissa having the same ordinate.
    Then the same procedure is repeated on the resulting graph for the top and bottom nodes.
    If identify_nodes is set to False, the nodes are just removed instead of being identified.
    """

    g = graph.copy() # deep copy of the graph
    
    pos = nx.get_node_attributes(g, 'para').values()
    # transpose
    x, y = zip(*pos)
    xmin = min(x) # leftmost abscissa
    xmax = max(x) # rightmost abscissa
    ymin = min(y) # bottommost ordinate
    ymax = max(y) # topmost ordinate
    
    # tolerance is one percent of the average distance between two nodes
    av_dist = min(xmax-xmin, ymax-ymin)/sqrt(len(x))    
    tol = 0.01*av_dist
    
    left_nodes = []
    right_nodes = []
    for p in g.nodes(data=True):
        px = p[1]['para'][0]
        if isclose(px, xmin, abs_tol=tol):
            left_nodes.append(p)
        if isclose(px, xmax, abs_tol=tol):
             right_nodes.append(p)
    # sort left and right nodes according to their y coordinate, so that left_n[i] and right_n[i] are facing each other
    left_nodes.sort(key=lambda p: p[1]['para'][1])
    right_nodes.sort(key=lambda p: p[1]['para'][1])
    
    if identify_nodes:
        # keep the nodes only
        left_nodes = list(zip(*left_nodes))[0]
        right_nodes = list(zip(*right_nodes))[0]
        # identify right nodes with left nodes
        identify_node_bunch(g, left_nodes, right_nodes)
    else:
        # remove right_nodes
        g.remove_nodes_from(right_nodes)
                    
    # now perform the same thing on the y axis
    
    top_nodes = []
    bottom_nodes = []
    for p in g.nodes(data=True):
        py = p[1]['para'][1]
        if isclose(py, ymin, abs_tol=tol):
            bottom_nodes.append(p)
        if isclose(py, ymax, abs_tol=tol):
             top_nodes.append(p)
    # sort top and bottom nodes according to their x coordinate
    top_nodes.sort(key=lambda p: p[1]['para'][0])
    bottom_nodes.sort(key=lambda p: p[1]['para'][0])
    
    if identify_nodes:
        # keep the nodes only
        bottom_nodes = list(zip(*bottom_nodes))[0]
        top_nodes = list(zip(*top_nodes))[0]
        # identify top nodes with bottom nodes
        identify_node_bunch(g, bottom_nodes, top_nodes)
    else:
        # remove top_nodes
        g.remove_nodes_from(top_nodes)
        
    return g
    
def OS_ham(graph, t, V):
    """
    Return the on-site potentials hamiltonian with couplings V(i) =  z(i)*V
    Remark: OS_ham(g, 1, 1) is equivalent to nx.laplacian_matrix(g)
    """

    ham = nx.to_numpy_matrix(graph)
    coord = [len(graph.neighbors(p)) for p in graph]
    #L = len(graph)
    #ind = range(L)
    #diag = sparse.csr_matrix((coord, (ind, ind)), (L, L))
    diag = np.diag(coord)
    
    return -t*ham + V*diag
    
def sparse_OS_ham(graph, t, V):
    """
    Return the on-site potentials hamiltonian with couplings V(i) =  z(i)*V
    Remark: OS_ham(g, 1, 1) is equivalent to nx.laplacian_matrix(g)
    """

    ham = nx.to_scipy_sparse_matrix(graph, dtype=float)
    coord = [len(graph.neighbors(p)) for p in graph]
    L = len(graph)
    ind = range(L)
    diag = csr_matrix((coord, (ind, ind)), (L, L))
    
    return -t*ham + V*diag
    
def ani_graph(graph, couplings, lift = None):
    """
    Return a graph with an anisotropic hopping data added to the edges
    If nodes of the graph are the the higher dimensional positions, a lift (ie dict from nodes to their higher dim pos) must be provided.
    couplings is a dict associating to each (higher dimensional) edge a hopping amplitude.
    """    
    
    g = graph.copy() # a graph whose edges have a "hopping" argument setting the corresponding hopping
    if lift:
        hop = {e:couplings[tuple(abs(lift[e[1]]-lift[e[0]]))] for e in g.edges()}
    else:
        hop = {e:couplings[abs(e[1]-e[0])] for e in g.edges()}
    nx.set_edge_attributes(g, "hopping", hop)
    
    return g
        
def weights(graph):
    """ return the weight data of the edges of a nx graph """

    return [edge[-1] for edge in graph.edges(data='weight')]
    
def plot(graph, **kwargs):
    if "s" in kwargs:
        node_size = kwargs["s"]
    else:
        node_size = 0
        
    dict_pos = nx.get_node_attributes(graph, 'para') 
    
    if "labels" in kwargs:
        labels = kwargs["labels"]
    else:
        labels = None
    
    if "weights" in kwargs:
        width = kwargs["weights"]
    else:
        width = 0.1
        
    if "lims" in kwargs:
        lims = kwargs["lims"]
        plt.xlim(lims[0])
        plt.ylim(lims[1])
#    else:
#        plt.axis('tight')
    plt.axes().set_aspect('equal')        
    
    nx.draw_networkx(graph, dict_pos, width=width, with_labels = labels, labels = labels, node_size = node_size)        
        
    if "savename" in kwargs:
        # if a dpi value is specified, save as png with these dpi settings
        if "dpi" in kwargs:
            plt.savefig(kwargs["savename"] + ".png", dpi = kwargs["dpi"])
        # if no dpi value is specified, save in vector format
        else:
            plt.savefig(kwargs["savename"] + ".pdf")
#    plt.show()
#    plt.close()
    
def saveplot(graph, savename="graph_plot.pdf", line_width=0.1, node_size=0):
    list_pos = [(v.real, v.imag) for v in graph.nodes()]
    dict_pos = dict(zip(graph.nodes(), list_pos))
    nx.draw_networkx(graph, dict_pos, width=line_width, with_labels = False, node_size = 0)
    plt.axes().set_aspect('equal')
    plt.savefig(savename)
    plt.close()
    
def k_neighbors(graph, node, k):
    """
    Return the set of nodes that are exactly at a distance k from node
    """

    new_neigh = {node} # node that are in the new coordination zone but not in the old one
    visited = {node} # nodes that have already been visited by the algorithm
    for i in range(k):
        new_neigh = set(nn for n in new_neigh for nn in nx.neighbors(graph, n) if nn not in visited)
        visited |= new_neigh
    
    return new_neigh
    
class SquareCell():
    """ 
    Class constructing and manipulating the Hamiltonian H(k) describing the square unit cell of a periodic system
    """
    
    def __init__(self, graph):
        """
        Initialize the unit cell from a graph (to be periodized)
        """
        self._graph = graph.copy() # copy of the input graph
        nx.set_edge_attributes(self._graph, 'h', 0.)
        nx.set_edge_attributes(self._graph, 'v', 0.)
        # periodize the graph, assuming the unit cell is a square
        periodize_along(self._graph, 'h')
        periodize_along(self._graph, 'v')
        
        # whether the bulk and edge Hamiltonians have been constructed or not
        self._computed_bulk_edges = False
        # whether the local environments have been computed or not
        self._computed_local_envs = False
        # whether the onsite Hamiltonians have been computed
        self._computed_onsite = False
        
    def __bulk_edges__(self):
        """
        Compute the bulk Hamiltonian
        Compute the edges Hamiltonians
        """
        # give a unique integer index to each node
        index = dict(zip(self._graph.nodes(), range(len(self._graph))))
        # dictionary holding the integer indices of each part of the edge Hamiltonian
        indices = {"h":{1:[],-1:[]},"v":{1:[],-1:[]},"hv":{1:[],-1:[]}}
        # list holding the integer indices of the bulk hamiltonian
        bulk_indices = []
        # realspace position of the nodes (used to determine the jump direction across the edges)
        pos = nx.get_node_attributes(self._graph, 'para')
        # loop over the jumps to affect the indices
        for p1, p2, attr in self._graph.edges(data=True):
            i = (index[p1], index[p2])
            # if the current jump is through an edge
            if attr['h'] or attr['v']:
                # the hopping changes depending on the direction we cross the edge
                dx, dy = np.sign(pos[p2] - pos[p1])
        
                if attr['h'] and attr['v']:
                    # check that in that case dx == dy
                    if dx != dy: raise RuntimeError("ioups!")
                    indices["hv"][dx].append(i)
                elif attr['h']:
                    indices["h"][dx].append(i)
                else:
                    indices["v"][dy].append(i)
            # if the current jump is a bulk jump
            else:
                bulk_indices.append(i)
        
        L = len(self._graph)
        self._Hedge_up = {"h":{}, "v":{}, "hv":{}}
        # construct the edge Hamiltonians
        for name in indices:
            # right movers
            for sign in (1,-1):
                if len(indices[name][sign])>0:
                    rows, cols = np.asarray(indices[name][sign]).T
                    data = np.ones(len(cols))
                    self._Hedge_up[name][sign] = csr_matrix((data, (rows, cols)), shape=(L,L))
                else:
                    self._Hedge_up[name][sign] = csr_matrix((L,L))
            
        # construct the bulk Hamiltonian
        rows, cols = np.asarray(bulk_indices).T
        data_bulk = np.ones(len(cols))
        Hbulk_up = csr_matrix((data_bulk, (rows, cols)), shape=(L,L))
        self._Hbulk = Hbulk_up + Hbulk_up.H
            
        self._computed_bulk_edges = True
                
    def H(self, t, k):
        """
        Return the Hamiltonian H(t, k) where k is the Bloch wavevector and t is the hopping amplitude
        """
        # if the bulk and edges Hamiltonians are not contructed, build them
        if not self._computed_bulk_edges:
            self.__bulk_edges__()
        # twists
        k = np.asarray(k)
        # if k is zero, we do not need complex numbers
        if k[0] == k[1] == 0:
            tx_r = ty_r = tx_l = ty_l = 1.
        #  else we do! (except for pi)
        else:
            tx_r, ty_r = np.exp(1j*k)
            tx_l, ty_l = np.exp(-1j*k)
        
        whole_edge_up = tx_r*self._Hedge_up["h"][1] + ty_r*self._Hedge_up["v"][1] + tx_r*ty_r*self._Hedge_up["hv"][1] + tx_l*self._Hedge_up["h"][-1] + ty_l*self._Hedge_up["v"][-1] + tx_l*ty_l*self._Hedge_up["hv"][-1]

        Hedge = whole_edge_up + whole_edge_up.H
        
        return t*(self._Hbulk + Hedge)
    
    def __compute_local_envs__(self):
        """
        list sites according to their local environment
        """
        zToEnv = {8:'A', 7:'B', 6:'C', 4:'E', 3:'F'}
        # D1 and D2 are distinguished by their local environment
        for pt in self._graph:
            n = self._graph.neighbors(pt)
            z = len(n)
            # if pt is a D type site, distinguish between D1 and D2
            if z == 5:
                # D2 sites have in their neighbourhood 2 F sites
                if 3 in [len(self._graph.neighbors(pn)) for pn in n]:
                    env = 'D2'
                else:
                    env = 'D1'
            else:
                env = zToEnv[z]
            self._graph.node[pt]['env'] = env
        self._computed_local_envs = True
        
    def __onsite__(self):
        """
        compute the on-site Hamiltonians that have a potential of 1 on the given local envs
        """
        if not self._computed_local_envs:
            self.__compute_local_envs__()
        
        # envs by points
        envs = nx.get_node_attributes(self._graph, 'env')
        
        # give a unique integer index to each node
        index = dict(zip(self._graph.nodes(), range(len(self._graph))))
        # number of nodes 
        L = len(self._graph)
        # integer indices of the positions of the D1 and D2 sites
        indices = {"A":[], "B":[], "C":[], "D1":[], "D2":[], "E":[], "F":[]}
        for pt in self._graph:
            env = envs[pt]            
            if env in indices.keys():
                indices[env].append(index[pt])
        self._Honsite = dict()
        for env in indices.keys():
            rows = indices[env]
            data = np.ones(len(rows))
            self._Honsite[env] = csr_matrix((data, (rows, rows)), shape=(L,L))
            
        self._computed_onsite = True
        
    def HD1D2(self, t, k, VD1, VD2):
        if not self._computed_onsite:
            self.__onsite__()
        return self.H(t, k) + VD1*self._Honsite["D1"] + VD2*self._Honsite["D2"]
        
    def spectrum(self, t, k):
        """
        Return the full energy spectrum of H(t, k) where k is the Bloch wavevector and t is the hopping amplitude
        """
        ham = self.H(t, k)
        return np.linalg.eigh(ham.todense())
        
    def states_around(self, t, k, E, nstates):
        """
        Find nstates states around energy E
        """
        ham = self.H(t, k)
        ens, vecs = eigsh(ham, k=nstates, sigma=E, which='LA') 
        return ens, vecs        
        
    def states_surround(self, t, k, E):
        """
        Find the 2 energy states surrounding energy E
        """
        ham = self.H(t, k)
        # state below
        en0, vec0 = eigsh(ham, k=1, sigma = E, which='SA')
        # state above
        en1, vec1 = eigsh(ham, k=1, sigma = E, which='LA')
    
        return ((en0[0], en1[0]), (vec0[:,0], vec1[:,0]))
        
    def state_above(self, t, k, E):
        """
        Find the energy state above energy E
        """
        ham = self.H(t, k)
        en, vec = eigsh(ham, k=1, sigma = E, which='LA')
    
        return (en[0], vec[:,0])
    
if __name__ == "__main__":
    import Tilings as tl
    import AB_envs as envs
    
    square = tl.A5(envs.squareCanonical)    
    n = 1
    square.it_sub(n)
        
    c = SquareCell(square._graph)
    mat = c.H(1., (np.pi/2., 0.))    
    ham = c.HD1D2(-1., (0.,0.), 1., 1.)
    
#    pg = c._graph
#    sites = [p for p in pg]    
#    pos =  nx.get_node_attributes(pg, 'para')
#    # color the D1 sites    
#    D1pts = c.Hos(1.,1.)
#    color = [(p in D1pts) for p in c._graph]
#    nx.draw_networkx(pg, pos, with_labels = False, node_size = 60., width = 1., node_color = color)
#    plt.axes().set_aspect('equal')
    
