#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 24 11:58:06 2017

@author: nicolas
"""

import numpy as np

sq5 = np.sqrt(5)
phi = (sq5 - 1.)/2.
pi = np.pi
e = np.exp(1j*pi*np.arange(5)/5)

def loz(orig, a, b):
    """ 
    create a lozenge with edges along a and b
    If thin (resp fat) lozenge is wanted, then (a, b) should be the acute (resp obtuse) angle of the lozenge
    """
    alpha = abs(np.angle(b/a))
    if np.around(alpha - pi/5, 2) == 0:
        color = 0
    elif np.around(alpha - 3*pi/5, 2) == 0:
        color = 1
    else:
        raise RuntimeError("The specified edges do not form an angle of pi/5 or 3*pi/5")

    A = orig
    B = orig + a
    C = orig + b

    return [(color, (A, B, C)), (color, (B + C - A, B, C))]

# fat rhombus
fat0 = loz(0j, e[0], e[3])
# thin rombus
thin0 = loz(0j, e[0], e[1])
