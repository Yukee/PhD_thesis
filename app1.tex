\section{Fractal dimensions}

In the following, we consider a KK eigenstate \eqref{eq:SKK}
\begin{equation}
	\psi_i = \psi_0(i) e^{\kappa h(i)}
\end{equation}
living on a quasiperiodic tiling that can be any of the examples considered previously: a substitution quasiperiodic chain, the Penrose tiling or the Ammann-Beenker tiling. 
We are going to prove that this KK state has a non-trivial multifractal spectrum, and is therefore critical.

We consider the sequence of regions constructed by repetitively applying the tiling inflation rule $\sigma$ on a initial region $\reg_0$: $\reg_t = \sigma^t \reg_0$.

After a few algebraic manipulations of the above definitions, we arrive at
\begin{equation}
	d_q(\psi) = \frac{1}{q-1} \lim_{t \to \infty} \log \left( \frac{Z^{(t)}(2 \kappa)^q}{Z^{(t)}(2q\kappa)} \right) / \log Z^{(t)}(0)
\end{equation}
where $Z$ is the partition function already introduced in the specific case of the Fibonacci chain \eqref{eq:partition}.
Here we have used the important assumption -- entering in the definition of a KK state \eqref{eq:SKK} -- that the height field is non-local, i.e. uncorrelated with the local configuration of the atoms. 

Furthermore, in all the cases considered here, the partition function has the scaling
\begin{equation}
	Z^{(t)}(\beta) \sim \omega^t(\beta).
\end{equation}
Therefore we arrive at the -- almost -- explicit expression of the fractal dimensions
\begin{equation}
	d_q(\psi) = \frac{1}{q-1} \log\left( \frac{\om(2 \kappa)^q}{\om(2 \kappa q)} \right)
\end{equation}
where the $\log$ is written in the basis such that $\log \omega(0) = 1$.
For the expression to be fully explicit, we need to compute $\om$ for the specific tiling at hand, as we already did in the Fibonacci case \eqref{eq:OmFibo}.

We can also compute the $f(\alpha)$ spectrum. 
Letting $x = 2\kappa$, we have
\begin{equation}
	\alpha_q = \log(\omega(x)) - x \frac{\omega'(qx)}{\omega(qx)}
\end{equation}
and
\begin{equation}
	f(\alpha_q) = \log(\omega(qx)) - qx \frac{\omega'(qx)}{\omega(qx)}
\end{equation}
Remark that whenever the function $\om$ is such that 
\begin{equation}
\label{eq:SymCond}
	\om(x) = e^{A x} \om(-x),
\end{equation}
the function $f(\alpha)$ is symmetric around its maximum.
Moreover, one can easily prove that the above condition \eqref{eq:SymCond} is equivalent to having a height distribution which is asymptotically symmetric around its maximum.
One can moreover express the maximum $h_M$ in terms of $A$: $h_M(t) = A t/2$